<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

abstract class ServerErrorException extends SDKException
{
    protected $httpResponseCode = 567;
}