<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Upload;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\ClientErrorException;

class NotAcceptableUploadException extends ClientErrorException
{

}