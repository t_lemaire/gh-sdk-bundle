<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;


/** DO NOT EXTEND THIS CLASS
 *
 *  Extend either ClientErrorException or ServerErrorException depending on the cause of the exception
 * @see https://jira.mgcorp.co/browse/IHT-2845 for more infomration on the reason
 */

abstract class SDKException extends \Exception
{
    protected $httpResponseCode = 500;

    public function getHttpResponseCode()
    {
        return $this->httpResponseCode;
    }

}