<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\FlipBookIdentifier;

/**
 * An instance of this class is thrown when an attempt is made to access the scores of the specified flipbook, but
 * no scores are available for that flipbook.
 */
class NoScoresForFlipBookException extends \Exception
{
    /**
     * NoScoresForFlipBookException constructor.
     * @param FlipBookIdentifier $flipBookIdentifier
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(FlipBookIdentifier $flipBookIdentifier, $code = 0, \Throwable $previous = null)
    {
        parent::__construct(
            "No scores in the result for the flipbook generated in operation "
					. $flipBookIdentifier->getFlipBookOperationId(). " with metrics "
					. $flipBookIdentifier->getMetricId() . ".",
            $code,
            $previous
        );
    }
}