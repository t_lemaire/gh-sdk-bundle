<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class InvalidResponseException extends SDKException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'Invalid response.';
        }

        parent::__construct($message, $code, $previous);
    }

}