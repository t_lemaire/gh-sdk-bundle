<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class NoOperationsFoundException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'No operations found.';
        }

        parent::__construct($message, $code, $previous);
    }

}