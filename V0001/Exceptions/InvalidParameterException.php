<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class InvalidParameterException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'Invalid parameter.';
        }

        parent::__construct($message, $code, $previous);
    }

}