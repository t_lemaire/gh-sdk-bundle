<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class NoUploadedJobFoundException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'No uploaded Job found.';
        }

        parent::__construct($message, $code, $previous);
    }

}