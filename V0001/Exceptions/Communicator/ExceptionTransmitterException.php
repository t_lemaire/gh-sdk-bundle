<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\CommunicatorException;

class ExceptionTransmitterException extends CommunicatorException
{
    private $type = '';

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}