<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\CommunicatorException;

class InvalidResponseException extends CommunicatorException
{

}