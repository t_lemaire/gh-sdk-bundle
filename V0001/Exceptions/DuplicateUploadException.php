<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class DuplicateUploadException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'This file or URL has been already uploaded';
        }

        parent::__construct($message, $code, $previous);
    }

}