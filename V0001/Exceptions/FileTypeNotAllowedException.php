<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;


class FileTypeNotAllowedException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'This File Type is not allowed.';
        }

        parent::__construct($message, $code, $previous);
    }
}