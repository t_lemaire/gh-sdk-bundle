<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

abstract class ClientErrorException extends SDKException
{
    protected $httpResponseCode = 467;
}