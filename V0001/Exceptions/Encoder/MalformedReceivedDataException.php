<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Encoder;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\ServerErrorException;

class MalformedReceivedDataException extends ServerErrorException
{

}