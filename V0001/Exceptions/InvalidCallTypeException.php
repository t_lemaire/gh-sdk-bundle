<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class InvalidCallTypeException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'The Job already exists.';
        }

        parent::__construct($message, $code, $previous);
    }

}