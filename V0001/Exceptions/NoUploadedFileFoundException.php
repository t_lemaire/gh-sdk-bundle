<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class NoUploadedFileFoundException extends ClientErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'No uploaded File found.';
        }

        parent::__construct($message, $code, $previous);
    }

}