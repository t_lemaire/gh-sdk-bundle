<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class UnableToSaveTheEncodeCleanupJobException extends ServerErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'Unable to save the encode cleanup job.';
        }

        parent::__construct($message, $code, $previous);
    }

}