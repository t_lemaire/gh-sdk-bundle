<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class UnexpectedVobileResponseException extends \Exception
{
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        if ($message === null) {
            $message = 'Unexpected response from Vobile.';
        }

        parent::__construct($message, $code, $previous);
    }

}