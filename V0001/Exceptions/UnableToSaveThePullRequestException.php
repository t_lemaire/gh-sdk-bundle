<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Exceptions;

class UnableToSaveThePullRequestException extends ServerErrorException
{

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        if ($message === '') {
            $message = 'Unable to save the Pull Request.';
        }

        parent::__construct($message, $code, $previous);
    }

}