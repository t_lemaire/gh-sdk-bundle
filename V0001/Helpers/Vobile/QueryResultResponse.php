<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Helpers\Vobile;

use GorillaHub\GeneralBundle\Time\TimeFunctions;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\UnexpectedVobileResponseException;
use SimpleXMLElement;

/**
 * A class of this type represents the status of a query.
 */
class QueryResultResponse
{
	/** @var SimpleXMLElement The <Query> node of the response from Vobile. */
	private $queryNode;

    /** @var string The XML from Vobile, where the root node is <Result>. */
    private $rawResponse;

	/**
	 * @param string $rawResponse The XML from Vobile, where the root node is <Result>.
     * @throws UnexpectedVobileResponseException
	 */
	public function __construct($rawResponse) {
        if ($rawResponse instanceof SimpleXMLElement) {
            $this->rawResponse = preg_replace(
                '`<\\?xml[^>]*>`',
                '',
                $rawResponse->asXML()
            );
            $root = $rawResponse;
        } else {
            $this->rawResponse = $rawResponse;
            $root = simplexml_load_string($rawResponse);
        }
        if (!$root instanceof SimpleXMLElement) {
            throw new UnexpectedVobileResponseException("Can't interpret the Vobile query result.");
        }
        if (!isset($root->Body->Query)) {
            throw new UnexpectedVobileResponseException("Can't interpret the Vobile query result.");
        }
		$this->queryNode = $root->Body->Query;
	}

    /**
     * @return string The XML from Vobile, where the root node is <Result>.
     */
	public function getRawResponse() {
        return $this->rawResponse;
    }

	/**
	* This function is used to determine if the video is known to be copyright by Vobile.  Do not
	* call this function unless isResultKnown() returns true.
	*
	* @return bool true if the video is copyright, false if not.
	* @throws \LogicException if the result is not known.
    * @throws UnexpectedVobileResponseException
	*/
	public function isCopyright() {

		$status = $this->getStatus();
		if ($status === 2) {
			throw new \LogicException(
					'isCopyright() was called before Vobile status was available. '
							. 'Use isResultKnown() before calling this method.'
					);
		}
		return $status === 1;
	}

	/**
	* This function determines whether Vobile has finished processing the video.
	*
	* @return bool true if the result is known, false otherwise.
	* @throws UnexpectedVobileResponseException
	*/
	public function isResultKnown() {

		$status = $this->getStatus();
		return $status !== 2;
	}

	/**
	* This function returns the name of the owner of the copyright, if known.  This should be
	* called only if isResultKnown() and isCopyright() return true (but it is safe to call in any case).
	*
	* @return string|null The name of the owner, or null if unknown.
	*/
	public function getOwner() {
		return isset($this->queryNode->Match->Asset->RightsOwner)
				? trim((string)$this->queryNode->Match->Asset->RightsOwner)
				: null;
	}


	/**
	 * @param string $requestedDetailName The name of the detail to return.
	 * @return string|null The value of the specified detail, or null if it is not specified.
	 */
	public function getMatchDetail($requestedDetailName) {
		if (!isset($this->queryNode->Match->MatchDetail->Track)) {
			return null;
		}
		$details = get_object_vars($this->queryNode->Match->MatchDetail->Track);
		return isset($details[$requestedDetailName]) ? trim((string)$details[$requestedDetailName]) : null;
	}

	/**
	 * @return string[] An array where, for each element, the key is the name of a detail and the value is
	 * 		the value of the detail.
	 */
	public function getAllMatchDetails() {
		if (!isset($this->queryNode->Match->MatchDetail->Track)) {
			return [];
		}
		$details = get_object_vars($this->queryNode->Match->MatchDetail->Track);
		unset($details['@attributes']);
		return array_map(
			function($d) { return trim((string)$d); },
			$details
		);
	}

	/**
	* This function returns the name of the copyright video, if known.  This should be called only
	* if isResultKnown() and isCopyright() return true (but it is safe to call in any case).
	*
	* @return string|null The name of the original video, or null if unknown.
	*/
	public function getTitle() {

		return isset($this->queryNode->Match->Asset->Title)
				? trim((string)$this->queryNode->Match->Asset->Title)
				: null;
	}
	/**
	* This function returns the release date of the copyright video, if known.  This should be called only
	* if isResultKnown() and isCopyright() return true (but it is safe to call in any case).
	*
	* @return string|null The release date of the original video, or null if unknown.
	*/
	public function getReleaseDate() {

		return isset($this->queryNode->Match->Asset->ReleaseDate)
				? trim((string)$this->queryNode->Match->Asset->ReleaseDate)
				: null;
	}

	/**
	* @return string|null This function returns the identifier for the file that was assigned by Vobile, or null if
	 * 		none.
	*/
	public function getFileId() {
		if (!isset($this->queryNode->QueryLog->File)) {
			return null;
		}

		$fileId = trim((string) $this->queryNode->QueryLog->File);
		if ($fileId === '') {
			return null;
		}
		return $fileId;
	}

	/**
	 * @return string This function returns the task ID of the query task.
	 * @throws UnexpectedVobileResponseException
	 */
	public function getTaskId() {
		$this->throwExceptionIfFailed();
		return trim((string) $this->queryNode->QueryLog->TaskID);
	}

	/**
	 * @return bool true if the query result was successfully received--although isKnown() may still return false
	 * 		and isCopyright() may still return true.
	 */
	public function isSuccessful() {
		return $this->getErrorMessage() === null;
	}

	/**
	 * @return null|string An error message, or null if the query result was successfully received--although,
	 * 		even if this returns null, isKnown() may still return false and isCopyright() may still return true.
	 */
	public function getErrorMessage() {
		if (!isset($this->queryNode->QueryLog->Status)) {
			return "Status was not specified.";
		}
		$rawStatus = trim((string) $this->queryNode->QueryLog->Status);
		if (!is_numeric($rawStatus)) {
			return "Unexpected status $rawStatus.";
		}
		$status = intval($rawStatus);
		if ($status < 0 || $status > 2) {
			return "Unexpected status $rawStatus.";
		}
		if (!isset($this->queryNode->QueryLog->TaskID)) {
			return "Task ID was not specified.";
		}
		if (trim((string)$this->queryNode->QueryLog->TaskID) === '') {
			return "Task ID was empty.";
		}
		return null;
	}

	public function throwExceptionIfFailed() {
		$errorMessage = $this->getErrorMessage();
		if ($errorMessage !== null) {
			throw new UnexpectedVobileResponseException($errorMessage);
		}
	}

	/**
	 * @return string The XML representation of the <Query> node of the response from Vobile.
	 */
	public function asXml() {
		$xml = $this->queryNode->asXML();
		return trim(preg_replace('`^\\s*<\\?xml.*?\\?>\\s*`', '', $xml));
	}

	/**
	 * This function returns the Vobile status code for the video.
	 *
	 * @return int The Vobile status code:
	 * 	0: Vobile does not recognize the video.
	 * 	1: Vobile recognizes the video as copyrighted.
	 *	2: The status is not yet available.
	 * @throws UnexpectedVobileResponseException
	 */
	private function getStatus() {
		$this->throwExceptionIfFailed();
		$rawStatus = trim((string) $this->queryNode->QueryLog->Status);
		return intval($rawStatus);
	}

	/**
	 * @return int The unix timestamp of the task.
	 * @throws UnexpectedVobileResponseException
	 */
	public function getTimestamp() {
		$this->throwExceptionIfFailed();
		if (isset($this->queryNode->QueryLog->TimeStamp) === false) {
			throw new UnexpectedVobileResponseException("TimeStamp not specified.");
		}
		$timeFunctions = new TimeFunctions('UTC');
		$timestamp = $timeFunctions->strtotime($this->queryNode->QueryLog->TimeStamp);
		if ($timestamp === false) {
			throw new UnexpectedVobileResponseException("Can't interpret TimeStamp.");
		}
		return $timestamp;
	}


}