<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Helpers;


/**
 * This class provides methods for creating JSON web signature headers.
 */
class JsonWebSignature
{
    /**
     * @param string $key A key shared between the caller and receiver.
     * @param int $ttl The number of seconds after which the signature should expire.
     * @return string[] An array of header values, indexed by header name, e.g. ['headername' => 'headervalue'].
     */
    static public function getAuthenticationHeaders($key, $ttl = 60) {
        $time = time();
        $token = [
            'iat' => $time,
            'nbf' => $time,
            'exp' => $time + $ttl
        ];
        $header = self::jwsEncode(['alg' => 'HS256', 'typ' => 'JWT']);
        $payload = self::jwsEncode($token);
        $hash = hash_hmac('SHA256', $header . '.' . $payload, $key, true);
        $bearer = implode('.', [$header, $payload, self::jwsEncode($hash)]);
        $headers['X-AuthToken'] = $bearer;
        return $headers;
    }

    /**
     * @param mixed $data The data to encode.  If this is not a string, the data is json_encoded.
     * @return string The encoded data.
     */
    static private function jwsEncode($data) {
        if (!is_string($data)) {
            $data = json_encode($data);
        }
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }
}