<?php

namespace GorillaHub\SDKs\SDKBundle\V0001;

class ExceptionTransmitter
{

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $line;

    /**
     * @var int
     */
    protected $code;

    /**
     * @param \Exception $e The exception to transmit.
     */
    public function __construct(\Exception $e = null)
    {
        if ($e !== null) {
            $this->setCode($e->getCode());
            $this->setFile($e->getFile());
            $this->setLine($e->getLine());
            $this->setMessage($e->getMessage());
            $this->setType(get_class($e));
        }
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param int $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    /**
     * @return \Exception The exception (with some details wrong, like trace, file, and line) that was transmitted
     *        by this class.
     */
    public function createException()
    {
        $type = $this->getType();
        return new $type($this->getMessage(), $this->getCode());
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

}