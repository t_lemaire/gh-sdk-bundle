<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

interface ResultInterface
{

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return $this
     */
    public function setSignature(Signature $signature);

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature();

    /**
     * Sets the original operation.
     *
     * @param OperationInterface $operation
     *
     * @return $this
     */
    public function setOriginalOperation(OperationInterface $operation);

    /**
     * Returns the original operation/
     *
     * @return OperationInterface
     */
    public function getOriginalOperation();

}