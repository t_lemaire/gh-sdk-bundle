<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\CallEventRecords;

class HttpCallEventRecord extends CallEventRecord
{
    /** @var string The URL that was to be called. */
    private $url;

    /** @var int|null The HTTP status code of the response, or null if none. */
    private $httpStatus = null;

    /**
     * @return int|null {@see $httpStatus}
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * @param int|null $httpStatus {@see $httpStatus}
     * @return $this
     */
    public function setHttpStatus($httpStatus)
    {
        $this->httpStatus = $httpStatus;
        return $this;
    }

    /**
     * @return string {@see $url}
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url {@see $url}
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }


}