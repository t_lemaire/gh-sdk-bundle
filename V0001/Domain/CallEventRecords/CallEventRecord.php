<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\CallEventRecords;

/**
 * This class records the details of a call, or an attempt to call.
 *
 * @package GorillaHub\SDKs\SDKBundle
 */
class CallEventRecord
{
    /** @var \DateTime The time at which the call was initiated. */
    private $startTime;

    /** @var string The data that was supposed to be sent to the target. */
    private $rawRequest = null;

    /**
     * @var string|null The data that was sent back to the caller, or null if the target did not
     *        respond.  Note that null indicates an error while an empty string indicates an empty response.
     */
    private $rawResponse = null;

    /** @var bool True iff the call was successful. */
    private $wasSuccessful = false;

    /** @var float The number of seconds taken to send the call before succeeding, failing, or timing out. */
    private $durationSeconds = 0;

    /** @var float The exact start time, used to determine the duration. */
    private $startMicroTime;

    public function __construct()
    {
        $this->startTime = new \DateTime();
        $this->startMicroTime = microtime(true);
    }

    /**
     * @param int|null $httpStatus {@see $httpStatus}
     * @return $this
     */
    public function setHttpStatus($httpStatus)
    {
        $this->httpStatus = $httpStatus;
        return $this;
    }

    /**
     * @return int|null {@see $httpStatus}
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * @return float {@see $durationSeconds}
     */
    public function getDurationSeconds()
    {
        return $this->durationSeconds;
    }

    /**
     * @param float|null $durationSeconds {@see $durationSeconds}.  This may be null, in which case the duration
     *        is set to the number of seconds for which this object has existed.
     */
    public function setDurationSeconds($durationSeconds = null)
    {
        $this->durationSeconds = ($durationSeconds !== null)
            ? $durationSeconds
            : microtime(true) - $this->startMicroTime;
    }

    /**
     * @return string {@see $rawRequest}
     */
    public function getRawRequest()
    {
        return $this->rawRequest;
    }

    /**
     * @param string $rawRequest {@see $rawRequest}
     * @return $this
     */
    public function setRawRequest($rawRequest)
    {
        $this->rawRequest = $rawRequest;
        return $this;
    }

    /**
     * @return null|string {@see $rawResponse}
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param null|string $rawResponse {@see $rawResponse}
     * @return $this
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
        return $this;
    }

    /**
     * @return \DateTime {@see $startTime}
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime {@see $startTime}
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return boolean {@see $wasSuccessful}
     */
    public function getWasSuccessful()
    {
        return $this->wasSuccessful;
    }

    /**
     * @param boolean $wasSuccessful {@see $wasSuccessful}
     * @return $this
     */
    public function setWasSuccessful($wasSuccessful)
    {
        $this->wasSuccessful = $wasSuccessful;
        return $this;
    }

    /**
     * This method interprets the raw request data as form-data and returns the key value pairs represented by that
     * form-data.
     *
     * @return string[] The keys and values represented by the raw request.
     */
    public function getRequestFormValues()
    {
        parse_str($this->rawRequest, $formValues);
        return $formValues;
    }


}