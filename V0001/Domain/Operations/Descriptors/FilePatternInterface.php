<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;

interface FilePatternInterface
{

    /**
     * @param FilePattern $filePattern
     */
    public function setFilePattern(FilePattern $filePattern);

    /**
     * @return FilePattern
     */
    public function getFilePattern();

}