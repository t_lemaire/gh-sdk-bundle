<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern;

use GorillaHub\FilesBundle\Paths;

/**
 * Class FilePattern
 * @package GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern
 */
class FilePattern
{
    /**
     * @var string
     */
    protected $basePath;
    /**
     * @var string
     */
    protected $pathPattern;
    /**
     * @var string
     */
    protected $filePattern;
    /**
     * @var string Alias name of the client storage volume
     */
    protected $volumeName = 'default';

    /**
     * @return string
     */
    public function getVolumeName()
    {
        return $this->volumeName !== null ? $this->volumeName : 'default';
    }

    /**
     * @param string $volumeName
     * @return FilePattern
     */
    public function setVolumeName($volumeName)
    {
        $this->volumeName = $volumeName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param mixed $basePath
     *
     * @return $this
     */
    public function setBasePath($basePath)
    {
        $this->basePath = Paths::removeLeadingSlash($this->cleanUp($basePath));

        return $this;
    }

    /**
     * Clean up any directory attempt of scaling up
     *
     * @param $value
     *
     * @return mixed
     */
    protected function cleanUp($value)
    {
        $value = str_replace('\\', '/', $value);

        return Paths::cleanPath($value);
    }

    /**
     * @return mixed
     */
    public function getFilePattern()
    {
        return $this->filePattern;
    }

    /**
     * @param mixed $filePattern
     *
     * @return $this
     */
    public function setFilePattern($filePattern)
    {
        $this->filePattern = $this->cleanUp($filePattern);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPathPattern()
    {
        return $this->pathPattern;
    }

    /**
     * @param mixed $pathPattern
     *
     * @return $this
     */
    public function setPathPattern($pathPattern)
    {
        $this->pathPattern = $this->cleanUp($pathPattern);

        return $this;
    }

    /**
     *
     * @param array $definition
     *
     * @return string
     */
    public function parseFilePattern(array $definition = array())
    {
        return $this->parse($this->filePattern);
    }

    /**
     * @param       $pattern
     *
     * @param array $definition
     *
     * @return string
     */
    protected function parse($pattern, array $definition = array())
    {
        return $pattern;
    }

    /**
     *
     * @param array $definition
     *
     * @return string
     */
    public function parsePathPattern(array $definition = array())
    {
        return $this->parse($this->pathPattern, $definition);
    }

	/**
	 * @param string $stringToPad
	 * @return string The string $stringToPad with the configured padding applied.
	 */
    public function getWithPadding($stringToPad) {
        return $stringToPad;
	}

	/**
	 * @return string The full path pattern, including base path, path (i.e. directory), and filename.
	 */
	public function getFullPattern() {
    	return Paths::joinPaths($this->getBasePath(), $this->getPathPattern(), $this->getFilePattern());
	}

	/**
	 * @return string The full path pattern of the directory, including base path and path (i.e. directory),
	 * 		but NOT the filename.
	 */
	public function getFullDirectoryPattern() {
    	return Paths::joinPaths($this->getBasePath(), $this->getPathPattern());
	}

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->basePath . $this->pathPattern . $this->filePattern;
    }

}