<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern;

/**
 * Class FilePadPattern
 * @package GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern
 */
class FilePadPattern extends FilePattern
{
    /**
     * @var int
     */
    protected $padLength = 0;
    /**
     * @var string
     */
    protected $padString = '0';
    /**
     * @var int
     */
    protected $padPosition = STR_PAD_RIGHT;

    /**
     * @return int
     */
    public function getPadLength()
    {
        return $this->padLength;
    }

    /**
     * @param int $padLength
     * @return FilePadPattern
     */
    public function setPadLength($padLength)
    {
        $this->padLength = $padLength;

        return $this;
    }

    /**
     * @return string
     */
    public function getPadString()
    {
        return $this->padString;
    }

    /**
     * @param string $padString
     * @return FilePadPattern
     */
    public function setPadString($padString)
    {
        $this->padString = $padString;

        return $this;
    }

    /**
     * @return int
     */
    public function getPadPosition()
    {
        return $this->padPosition;
    }

    /**
     * @param int $padPosition
     * @return FilePadPattern
     */
    public function setPadPosition($padPosition)
    {
        if (!in_array($padPosition, [STR_PAD_BOTH, STR_PAD_LEFT, STR_PAD_RIGHT])) {
            throw new \Exception("Invalid pad position");
        }
        $this->padPosition = $padPosition;

        return $this;
    }

	/**
	 * @param string $stringToPad
	 * @return string The string $stringToPad with the configured padding applied.
	 */
    public function getWithPadding($stringToPad) {
        return str_pad(
    		$stringToPad,
			$this->getPadLength(),
			$this->getPadString(),
			$this->getPadPosition()
		);
	}
}