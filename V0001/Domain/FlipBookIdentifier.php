<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

/**
 * An object of this type identifies a specific flipbook within a VideoJob.
 */
class FlipBookIdentifier
{
	/** @var string The ID of the FlipBookOperation that is to generate the flipbook. */
	private $flipBookOperationId;

	/** @var string The metricId of the FlipBookMetrics that describes the specific flipbook to be generated. */
	private $metricId;

	/**
	 * @param string|null $flipBookOperationId The ID of the FlipBookOperation that is to generate the flipbook,
	 * 		or null if it is to be set later with setFlipBookOperationId().
	 * @param string|null $metricId The metricId of the FlipBookMetrics that describes the specific flipbook to be
	 * 		generated, or null if it is to be set later with setMetricId().
	 */
	public function __construct($flipBookOperationId = null, $metricId = null) {
		$this->flipBookOperationId = $flipBookOperationId;
		$this->metricId = $metricId;
	}

	public function isSameAs(FlipBookIdentifier $other) {
		return $this->flipBookOperationId === $other->flipBookOperationId
				&& $this->metricId === $other->metricId;
	}

	/**
	 * @return string @see $flipBookOperationId
	 */
	public function getFlipBookOperationId()
	{
		return $this->flipBookOperationId;
	}

	/**
	 * @param string $flipBookOperationId @see $flipBookOperationId
	 * @return $this
	 */
	public function setFlipBookOperationId($flipBookOperationId)
	{
		$this->flipBookOperationId = $flipBookOperationId;
		return $this;
	}

	/**
	 * @return string @see $metricId
	 */
	public function getMetricId()
	{
		return $this->metricId;
	}

	/**
	 * @param string $metricId @see $metricId
	 * @return $this
	 */
	public function setMetricId($metricId)
	{
		$this->metricId = $metricId;
		return $this;
	}



}