<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

interface DateTimeInterface
{
    /**
     * @param string $datetime
     *
     * @return $this
     */
    public function setISO8601($datetime);

    /**
     * @return int
     */
    public function getISO8601();

    /**
     * @return \DateTime
     */
    public function toDateTime();

}