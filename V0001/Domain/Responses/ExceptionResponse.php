<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Responses;

use GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter;

class ExceptionResponse extends Response
{

    /**
     * @var \GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter
     */
    private $exception;

    /**
     * @return \GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param \GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter $exception
     *
     * @return $this
     */
    public function setException(ExceptionTransmitter $exception)
    {
        $this->exception = $exception;

        return $this;
    }

}