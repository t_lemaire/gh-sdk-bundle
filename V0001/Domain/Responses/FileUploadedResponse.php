<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Responses;

class FileUploadedResponse extends Response
{

    private $fileName = '';

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

}