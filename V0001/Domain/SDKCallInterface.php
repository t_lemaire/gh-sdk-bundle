<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

interface SDKCallInterface
{

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature);

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature();

}