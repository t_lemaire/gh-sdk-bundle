<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\FlipBookIdentifier;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\NoScoresForFlipBookException;

/**
 * This is a set of scores that was generated for one particular flipbook in response to a ScoreFlipBooksOperation.
 * See ScoreFlipBooksOperation and ScoreFlipBooksResults for details.
 */
class FlipBookScores
{
	/** @var FlipBookIdentifier The identifier of the flipbook to which the scores apply. */
	private $flipBookIdentifier;

	/**
	 * @var float[] One score per thumbnail, indexed by thumbnail number.  The thumbnail numbers are indicated by
	 * 		the PatternRange included in the corresponding FlipBookResult.  For example, if the PatternRange start
	 * 		is 1 and its end is 14, then the keys here will be 1 to 14.  Note that some (or all) thumbnails may not
	 * 		have scores, so this array can be any size up to the number of thumbnails.  If any key is missing, the
	 * 		key will exist in $errorMessages.
	 */
	private $scores;

	/**
	 * @var string[] One error message, per thumbnail, indexed by thumbnail number (@see $scores).  If any key is
	 * 		missing, the key will exist in $scores.
	 */
	private $errorMessages;

	/**
	 * @return FlipBookIdentifier @see $flipBookIdentifier
	 */
	public function getFlipBookIdentifier()
	{
		return $this->flipBookIdentifier;
	}

	/**
	 * @param FlipBookIdentifier $flipBookIdentifier @see $flipBookIdentifier
	 * @return $this
	 */
	public function setFlipBookIdentifier($flipBookIdentifier)
	{
		$this->flipBookIdentifier = $flipBookIdentifier;
		return $this;
	}

	/**
	 * @return float[] @see $scores
	 */
	public function getScores()
	{
		return $this->scores;
	}

	/**
	 * @param float[] $scores @see $scores
	 * @return $this
	 */
	public function setScores($scores)
	{
		$this->scores = $scores;
		return $this;
	}

	/**
	 * @return string[] @see $errorMessages
	 */
	public function getErrorMessages()
	{
		return $this->errorMessages;
	}

	/**
	 * @param string[] $errorMessages @see $errorMessages
	 * @return $this
	 */
	public function setErrorMessages($errorMessages)
	{
		$this->errorMessages = $errorMessages;
		return $this;
	}


	/**
	 * @return int The first thumbnail number in either the scores or error messages.  You can use this in a loop
	 * 		like for ($i = $result->getFirstThumbnailNumber() ; $i <= $result->getLastThumbnailNumber() ; $i++).
	 */
	public function getFirstThumbnailNumber() {
		reset($this->errorMessages);
		reset($this->scores);
		return min(key($this->errorMessages), key($this->scores));
	}

	/**
	 * @return int The last thumbnail number in either the scores or error messages.  You can use this in a loop
	 * 		like for ($i = $result->getFirstThumbnailNumber() ; $i <= $result->getLastThumbnailNumber() ; $i++).
	 */
	public function getLastThumbnailNumber() {
		end($this->errorMessages);
		end($this->scores);
		return max(key($this->errorMessages), key($this->scores));
	}

	/**
	 * @return bool True iff at least one thumbnail was scored.  In other words, false iff getBestThumbnail() would
	 * 		throw an exception.
	 */
	public function wereAnyThumbnailsScored() {
		return empty($this->scores) === false;
	}

	/**
	 * @return int The thumbnail number of one of the thumbnails that has the highest score.
	 * @throws NoScoresForFlipBookException.  You can use wereAnyThumbnailsScored() to avoid having to catch
	 * 		this exception.
	 */
	public function getNumberOfBestThumbnail() {
		$bestScore = null;
		$bestThumbnailNumber = null;
		foreach ($this->scores as $thumbnailNumber => $score) {
			if ($bestScore === null || $score > $bestScore) {
				$bestScore = $score;
				$bestThumbnailNumber = $thumbnailNumber;
			}
		}
		if ($bestThumbnailNumber === null) {
			throw new NoScoresForFlipBookException($this->flipBookIdentifier);
		}
		return $bestThumbnailNumber;
	}

}