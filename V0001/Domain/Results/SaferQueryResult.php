<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

abstract class SaferQueryResult extends Result
{
    /**
     * @var string The message from Safer describing the match, or absence of a match.
     */
    private $responseJson;

    /**
     * @return bool True if there was a match.
     */
    public function wasMatched() {
        $response = $this->getResponse();
        return isset($response->reasons) && in_array('matched', $response->reasons);
    }

    /**
     * @return bool True if the video was classified as CSAM, pornography, or "other".
     */
    public function wasClassified() {
        $response = $this->getResponse();
        return isset($response->reasons) && in_array('classified', $response->reasons);
    }

    /**
     * @return float The CSAM classifier prediction score.
     */
    public function getCsamClassifierPrediction() {
        $response = $this->getResponse();
        return $response->classifierPrediction->csam ?? 0.0;
    }

    /**
     * @return object The message from Safer describing the match, or absence of a match.
     */
    public function getResponse()
    {
        return json_decode($this->responseJson);
    }


    /**
     * @return string The message from Safer describing the match, or absence of a match.
     */
    public function getResponseJson()
    {
        return $this->responseJson;
    }

    /**
     * @param string $responseJson The message from Safer describing the match, or absence of a match.
     * @return $this
     */
    public function setResponseJson($responseJson)
    {
        $this->responseJson = $responseJson;
        return $this;
    }



}
