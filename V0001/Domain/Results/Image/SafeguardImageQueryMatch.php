<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;

/**
 * An instance of this class describes a match between an uploaded image and an image that is already in the
 * Safeguard database.  You get these by calling getMatches() on a SafeguardImageQueryResult.
 */
class SafeguardImageQueryMatch
{
    /**
     * @var string One of "hash", "face", or "dph"--but others may be added in the future.
     *      This indicates the algorithm that was used to detect the match.
     */
    private $type;

    /** @var float The distance between the original image and the matched image. */
    private $distance;

    /** @var string The name of the client (on the Video Match Service) that registered the image. */
    private $clientName;

    /** @var int The integer ID of the client (on the Video Match Service) that registered the image. */
    private $clientId;

    /** @var string The string ID given to the image by the client that registered the image. */
    private $imageId;

    /** @var object|null An object that was given by the client when the image was registered, or null if none. */
    private $passthrough;

    /**
     * @return string One of "hash", "face", or "dph"--but others may be added in the future.
     *      This indicates the algorithm that was used to detect the match.
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type One of "hash", "face", or "dph"--but others may be added in the future.
     *      This indicates the algorithm that was used to detect the match.
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    /**
     * @return float The distance between the original image and the matched image.
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param float $distance The distance between the original image and the matched image.
     * @return $this
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * @return string The name of the client (on the Video Match Service) that registered the image.
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName The name of the client (on the Video Match Service) that registered the image.
     * @return $this
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
        return $this;
    }

    /**
     * @return int The integer ID of the client (on the Video Match Service) that registered the image.
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId The integer ID of the client (on the Video Match Service) that registered the image.
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return string The string ID given to the image by the client that registered the image.
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param string $imageId The string ID given to the image by the client that registered the image.
     * @return $this
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
        return $this;
    }

    /**
     * @return null|object An object that was given by the client when the image was registered, or null if none.
     */
    public function getPassthrough()
    {
        return $this->passthrough;
    }

    /**
     * @param null|object $passthrough An object that was given by the client when the image was registered, or null if
     *      none.
     * @return $this
     */
    public function setPassthrough($passthrough)
    {
        $this->passthrough = $passthrough;
        return $this;
    }

}
