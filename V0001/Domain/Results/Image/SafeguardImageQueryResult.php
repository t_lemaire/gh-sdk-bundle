<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * This result is sent back to you in response to a SafeguardImageQueryOperation, if that operation succeeds.  You can
 * call getResult() to get the exact result from Safeguard, but you should interpret the result by calling getOutcome()
 * and getMatches().
 */
class SafeguardImageQueryResult extends Result
{
    /** @var string No match was found. */
    const OUTCOME_CLEAR = "clear";

    /** @var string A match was found. */
    const OUTCOME_MATCH = "match";

    /** @var string All available algorithms were tried but no conclusive result was obtained. */
    const OUTCOME_SUSPICIOUS = "suspicious";

    /** @var string One of the OUTCOME_* constants. */
    private $outcome;

    /**
     * @var SafeguardImageQueryMatch[]
     */
    private $matches;

    /**
     * @var object The "result" object from Safeguard.  This can be archived for diagnostic purposes but it
     *      is not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     */
    private $result;

    /** @var string The ID assigned by Safeguard to this query. */
    private $safeguardQueryToken;

    /**
     * @var bool True iff any of the matches satisfy the current "standard" threshold setting (which is set uniformly
     *      for all clients in the Tubes CMS).
     */
    private $standardThresholdMatch = false;

    /**
     * @return string One of the OUTCOME_* constants.
     */
    public function getOutcome()
    {
        return $this->outcome;
    }

    /**
     * @param string $outcome One of the OUTCOME_* constants.
     * @return $this
     */
    public function setOutcome($outcome)
    {
        $this->outcome = $outcome;
        return $this;
    }

    /**
     * @return SafeguardImageQueryMatch[]
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * @param SafeguardImageQueryMatch[] $matches
     * @return $this
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;
        return $this;
    }



    /**
     * @return object The "result" object from Safeguard.  This can be archived for diagnostic purposes but it is
     *      not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     */
    public function getResult()
    {
        return $this->result;
    }

    
    /**
     * @param object $result The "result" object from Safeguard.  This can be archived for diagnostic purposes but it
     *      is not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }


    /**
     * @return string The ID assigned by Safeguard to this query.
     */
    public function getSafeguardQueryToken()
    {
        return $this->safeguardQueryToken;
    }

    /**
     * @param string $safeguardQueryToken The ID assigned by Safeguard to this query.
     * @return $this
     */
    public function setSafeguardQueryToken($safeguardQueryToken)
    {
        $this->safeguardQueryToken = $safeguardQueryToken;
        return $this;
    }

    /**
     * @return boolean True iff any of the matches satisfy the current "standard" threshold setting (which is set
     *      uniformly for all clients in the Tubes CMS).
     */
    public function getStandardThresholdMatch()
    {
        return $this->standardThresholdMatch;
    }

    /**
     * @param boolean $standardThresholdMatch True iff any of the matches satisfy the current "standard" threshold
     *      setting (which is set uniformly for all clients in the Tubes CMS).
     * @return $this
     */
    public function setStandardThresholdMatch($standardThresholdMatch)
    {
        $this->standardThresholdMatch = $standardThresholdMatch;
        return $this;
    }


}
