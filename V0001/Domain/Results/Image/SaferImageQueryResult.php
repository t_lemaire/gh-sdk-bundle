<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\SaferQueryResult;

/**
 * This result is sent back to you in response to a SaferImageQueryOperation, if that operation succeeds.  You can
 * call getResponse() to get the exact message from Safer.  You can use the convenience methods to interpret the
 * response.
 */
class SaferImageQueryResult extends SaferQueryResult
{

}
