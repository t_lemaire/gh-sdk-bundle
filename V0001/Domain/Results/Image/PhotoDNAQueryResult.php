<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * This result is sent back to you in response to a PhotoDNAQueryOperation, if that operation succeeds.  You can
 * call getRawResponse() to get the exact result from the PhotoDNA service, or you can call getIsMatch() to determine
 * if there was a match.
 */
class PhotoDNAQueryResult extends Result
{

    /** @var bool True iff there was a match. */
    private $isMatch;

    /**
     * @var string The uninterpreted response from the PhotoDNA service.
     */
    private $rawResponse;

    /**
     * @var string An ID assigned to this job by the PhotoDNA service.
     */
    private $trackingId;

    /**
     * @return bool True iff there was a match.
     */
    public function getIsMatch()
    {
        return $this->isMatch;
    }

    /**
     * @param bool $isMatch True iff there was a match.
     * @return $this
     */
    public function setIsMatch($isMatch)
    {
        $this->isMatch = $isMatch;
        return $this;
    }

    /**
     * @return string The uninterpreted response from the PhotoDNA service.
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param string $rawResponse The uninterpreted response from the PhotoDNA service.
     * @return $this
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
        return $this;
    }

    /**
     * @return string An ID assigned to this job by the PhotoDNA service.
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @param string $trackingId An ID assigned to this job by the PhotoDNA service.
     * @return $this
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
        return $this;
    }


}