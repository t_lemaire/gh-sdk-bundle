<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class GoogleContentSafetyQueryResult extends Result
{
    const REVIEW_PRIORITY_VERY_LOW = "VERY_LOW";
    const REVIEW_PRIORITY_LOW = "LOW";
    const REVIEW_PRIORITY_MEDIUM = "MEDIUM";
    const REVIEW_PRIORITY_HIGH = "HIGH";
    const REVIEW_PRIORITY_VERY_HIGH = "VERY_HIGH";

    /**
     * @var string Review priority suggested by Google.  One of the REVIEW_PRIORITY_* constants.
     */
    private $reviewPriority;

    /**
     * @return string Review priority suggested by Google.  One of the REVIEW_PRIORITY_* constants.
     */
    public function getReviewPriority()
    {
        return $this->reviewPriority;
    }

    /**
     * @param string $reviewPriority Review priority suggested by Google.  One of the REVIEW_PRIORITY_* constants.
     * @return $this
     */
    public function setReviewPriority($reviewPriority)
    {
        $this->reviewPriority = $reviewPriority;
        return $this;
    }
}
