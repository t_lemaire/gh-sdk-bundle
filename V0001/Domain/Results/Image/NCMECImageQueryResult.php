<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\NCMECQueryResult;

/**
 * This result is sent back to you in response to a NCMECImageQueryOperation, if that operation succeeds.  You can
 * call getResponse() to get the exact message from NCMEC.  You can use the convenience methods to interpret the
 * response.
 */
class NCMECImageQueryResult extends NCMECQueryResult
{

}