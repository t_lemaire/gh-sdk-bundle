<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * If a SubscribeToSafeguardImageQueryOperation is successful, then this result is sent in the corresponding
 * SuccessCall.  Note that no information about matches (or absence thereof) is actually returned, because that
 * information will be sent to the site through the site's subscription callback URL instead.  See
 * SubscribeToSafeguardImageQueryOperation for more details.
 */
class SubscribeToSafeguardImageQueryResult extends Result
{
    /** @var string The ID assigned by Safeguard to this query. */
    private $safeguardQueryToken;

    /**
     * @return string The ID assigned by Safeguard to this query.
     */
    public function getSafeguardQueryToken()
    {
        return $this->safeguardQueryToken;
    }

    /**
     * @param string $safeguardQueryToken The ID assigned by Safeguard to this query.
     * @return $this
     */
    public function setSafeguardQueryToken($safeguardQueryToken)
    {
        $this->safeguardQueryToken = $safeguardQueryToken;
        return $this;
    }
}
