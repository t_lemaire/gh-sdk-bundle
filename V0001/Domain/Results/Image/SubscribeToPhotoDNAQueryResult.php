<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Image;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * If a SubscribeToPhotoDNAQueryOperation is successful, then this result is sent in the corresponding
 * SuccessCall.  Note that no information about matches (or absence thereof) is actually returned, because that
 * information will be sent to the site through the site's subscription callback URL instead.  See
 * SubscribeToPhotoDNAQueryOperation for more details.
 */
class SubscribeToPhotoDNAQueryResult extends Result
{

}
