<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

abstract class NCMECQueryResult extends Result
{
    /**
     * @var string The message from NCMEC describing the matches, or absence of a match.
     */
    private $responseJson;

    /**
     * @return bool True if there was a match.
     */
    public function wasMatched()
    {
        return $this->getMatches() !== [];
    }

    /**
     * @return object[] An array of objects describing matched images or videos.
     */
    public function getMatches() {
        $response = $this->getResponse();
        return isset($response->ncmec_matches) ? $response->ncmec_matches : [];
    }


    /**
     * @return object The message from NCMEC describing the matches, or absence of a match.
     */
    public function getResponse()
    {
        return json_decode($this->responseJson);
    }


    /**
     * @return string The message from NCMEC describing the matches, or absence of a match.
     */
    public function getResponseJson()
    {
        return $this->responseJson;
    }

    /**
     * @param string $responseJson The message from NCMEC describing the matches, or absence of a match.
     * @return $this
     */
    public function setResponseJson($responseJson)
    {
        $this->responseJson = $responseJson;
        return $this;
    }



}
