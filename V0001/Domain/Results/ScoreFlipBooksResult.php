<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\FlipBookIdentifier;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\NoScoresForFlipBookException;

/**
 * This is a result that is sent to the client in response to a ScoreFlipBooksOperation.  One or more results may
 * be sent, each including some subset of the FlipBookScores objects.  If a flipbook fails to be generated, then
 * no FlipBookScores will be sent for that flipbook, even if scores for it were requested.  See
 * ScoreFlipBooksOperation for details.
 */
class ScoreFlipBooksResult extends Result
{
	/** @var FlipBookScores[] All sets of scores that are part of this result, indexed numerically. */
	private $flipBooksScores = [];

	/**
	 * @return FlipBookScores[] @see $flipBooksScores
	 */
	public function getFlipBooksScores()
	{
		return $this->flipBooksScores;
	}

	/**
	 * @param FlipBookScores[] $flipBooksScores @see $flipBooksScores
	 * @return $this
	 */
	public function setFlipBooksScores($flipBooksScores)
	{
		$this->flipBooksScores = $flipBooksScores;
		return $this;
	}


	/**
	 * @param FlipBookIdentifier $flipBookIdentifier
	 * @return bool True iff results exist for the specified FlipBook.  In other words, false iff
	 * 		getFlipBookScores() will not throw an exception given the specified identifier.
	 */
	public function wasFlipBookScored(FlipBookIdentifier $flipBookIdentifier) {
		try {
			$this->getFlipBookScores($flipBookIdentifier);
			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * @param FlipBookIdentifier $flipBookIdentifier
	 * @return FlipBookScores The scores of the specified flipbook.
	 * @throws NoScoresForFlipBookException.  You can use wasFlipBookScored() to avoid having to catch this exception.
	 */
	public function getFlipBookScores(FlipBookIdentifier $flipBookIdentifier) {
		foreach ($this->flipBooksScores as $flipBookScores) {
			if ($flipBookScores->getFlipBookIdentifier()->isSameAs($flipBookIdentifier)) {
				return $flipBookScores;
			}
		}
		throw new NoScoresForFlipBookException($flipBookIdentifier);
	}

}