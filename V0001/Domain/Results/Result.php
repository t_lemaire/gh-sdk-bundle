<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\ResultInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

/**
 * Class Result
 * @package GorillaHub\SDKs\SDKBundle\V0001\Domain
 */
abstract class Result implements ResultInterface
{

    /**
     * @var Signature
     */
    private $signature;

    /**
     * @var OperationInterface
     */
    private $originalOperation;

    /**
     * @var string
     */
    private $encodingType = 'mindgeek';

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    final public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return $this
     */
    final public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Returns the original operation for the result.
     *
     * @return OperationInterface
     */
    final public function getOriginalOperation()
    {
        return $this->originalOperation;
    }

    /**
     * Sets the original operation for the result.
     *
     * @param OperationInterface $operation
     *
     * @return $this
     */
    final public function setOriginalOperation(OperationInterface $operation)
    {
        $this->originalOperation = $operation;

        return $this;
    }

    /**
     * @return string
     */
    public function getEncodingType()
    {
        return $this->encodingType;
    }

    /**
     * @param string $encodingType
     * @return $this
     */
    public function setEncodingType($encodingType)
    {
        $this->encodingType = $encodingType;

        return $this;
    }

}