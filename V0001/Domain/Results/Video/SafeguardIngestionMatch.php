<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

/**
 * If duplicate content was found when ingesting an image or video into a Safeguard database, an instance of this
 * class describes one of the images or videos that already existed in the
 */
class SafeguardIngestionMatch
{
    /** @var string The ID of the image or video that matched, assigned by the client. */
    private $contentId;

    /** @var string The name of the client (on the Video Match Service) that registered the image or video. */
    private $clientName;

    /** @var int The ID of the client (on the Video Match Service) that registered the image or video. */
    private $clientId;

    /**
     * @var object|null The passthrough object given by the client when the image or video was registered, or
     *      null if none.
     */
    private $passthrough = null;

    /**
     * @return string The ID of the image or video that matched, assigned by the client.
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * @param string $contentId The ID of the image or video that matched, assigned by the client.
     * @return $this
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;
        return $this;
    }

    /**
     * @return string The name of the client (on the Video Match Service) that registered the image or video.
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName The name of the client (on the Video Match Service) that registered the image or
     *      video.
     * @return $this
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
        return $this;
    }

    /**
     * @return int The ID of the client (on the Video Match Service) that registered the image or video.
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId The ID of the client (on the Video Match Service) that registered the image or video.
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return object|null The passthrough object given by the client when the image or video was registered, or
     *      null if none.
     */
    public function getPassthrough()
    {
        return $this->passthrough;
    }

    /**
     * @param object|null $passthrough The passthrough object given by the client when the image or video was
     *      registered, or null if none.
     * @return $this
     */
    public function setPassthrough($passthrough)
    {
        $this->passthrough = $passthrough;
        return $this;
    }


}