<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\SaferQueryResult;

/**
 * This result is sent back to you in response to a SaferVideoQueryOperation, if that operation succeeds.  You can
 * call getResponse() to get the exact message from Safer.  You can use the convenience methods to interpret the
 * response.
 */
class SaferVideoQueryResult extends SaferQueryResult
{

}
