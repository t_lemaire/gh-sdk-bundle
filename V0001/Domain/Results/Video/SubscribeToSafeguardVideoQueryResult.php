<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;

/**
 * If a SubscribeToSafeguardVideoQueryOperation is successful, then this result is sent in the corresponding
 * SuccessCall.  Note that no information about matches (or absence thereof) is actually returned, because that
 * information will be sent to the site through the site's subscription callback URL instead.  See
 * SubscribeToSafeguardVideoQueryOperation for more details.
 */
class SubscribeToSafeguardVideoQueryResult extends VideoResult
{
    /** @var string The ID assigned by Safeguard to this query. */
    private $safeguardQueryToken;

    /**
     * @return string The ID assigned by Safeguard to this query.
     */
    public function getSafeguardQueryToken()
    {
        return $this->safeguardQueryToken;
    }

    /**
     * @param string $safeguardQueryToken The ID assigned by Safeguard to this query.
     * @return $this
     */
    public function setSafeguardQueryToken($safeguardQueryToken)
    {
        $this->safeguardQueryToken = $safeguardQueryToken;
        return $this;
    }


}
