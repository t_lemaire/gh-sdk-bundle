<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class FailResult extends Result
{

    /**
     * @var string
     */
    private $status = 'failed';

    /**
     * @var string
     */
    private $reason = '';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @return string
     */
    final public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    final public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    final public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     *
     * @return $this
     */
    final public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return string
     */
    final public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    final public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

}