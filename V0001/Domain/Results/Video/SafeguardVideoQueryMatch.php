<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

/**
 * An instance of this class describes a match between an uploaded video and a video that is already in the
 * Safeguard database.  You get these by calling getMatches() on a SafeguardVideoQueryResult.
 */
class SafeguardVideoQueryMatch
{
    /**
     * @var string One of "video_hash", "audio_hash", "mass", or "keyframe"--but others may be added in the future.
     *      This indicates the algorithm that was used to detect the match.
     */
    private $type;

    /** @var float The number of seconds that match. */
    private $secondsMatched;

    /** @var float The percentage of the duration of the video that match. */
    private $percentageMatched;

    /** @var string The name of the client (on the Video Match Service) that registered the video. */
    private $clientName;

    /** @var int The integer ID of the client (on the Video Match Service) that registered the video. */
    private $clientId;

    /** @var string The string ID given to the video by the client that registered the video. */
    private $videoId;

    /** @var object|null An object that was given by the client when the video was registered, or null if none. */
    private $passthrough;

    /**
     * @return string One of "video_hash", "audio_hash", "mass", or "keyframe"--but others may be added in the future.
     *      This indicates the algorithm that was used to detect the match.
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type One of "video_hash", "audio_hash", "mass", or "keyframe"--but others may be added in the
     *      future.  This indicates the algorithm that was used to detect the match.
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    /**
     * @return float The number of seconds that match.
     */
    public function getSecondsMatched()
    {
        return $this->secondsMatched;
    }

    /**
     * @param float $secondsMatched The number of seconds that match.
     * @return $this
     */
    public function setSecondsMatched($secondsMatched)
    {
        $this->secondsMatched = $secondsMatched;
        return $this;
    }

    /**
     * @return float The percentage of the duration of the video that match.
     */
    public function getPercentageMatched()
    {
        return $this->percentageMatched;
    }

    /**
     * @param float $percentageMatched The percentage of the duration of the video that match.
     * @return $this
     */
    public function setPercentageMatched($percentageMatched)
    {
        $this->percentageMatched = $percentageMatched;
        return $this;
    }

    /**
     * @return string The name of the client (on the Video Match Service) that registered the video.
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName The name of the client (on the Video Match Service) that registered the video.
     * @return $this
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
        return $this;
    }

    /**
     * @return int The integer ID of the client (on the Video Match Service) that registered the video.
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId The integer ID of the client (on the Video Match Service) that registered the video.
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return string The string ID given to the video by the client that registered the video.
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId The string ID given to the video by the client that registered the video.
     * @return $this
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
        return $this;
    }

    /**
     * @return null|object An object that was given by the client when the video was registered, or null if none.
     */
    public function getPassthrough()
    {
        return $this->passthrough;
    }

    /**
     * @param null|object $passthrough An object that was given by the client when the video was registered, or null
     *      if none.
     * @return $this
     */
    public function setPassthrough($passthrough)
    {
        $this->passthrough = $passthrough;
        return $this;
    }





}
