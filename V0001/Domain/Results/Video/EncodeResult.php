<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;

class EncodeResult extends VideoResult
{

    /**
     * @var string
     */
    private $format;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $resolution;

    /**
     * @var float
     */
    private $aspect;

    /**
     * @var string
     */
    private $videoBitRate;

    /**
     * @var string
     */
    private $audioBitRate;

    /**
     * @var string
     */
    private $audioChannel;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * @var string
     */
    private $previewUrl;

    /**
     * @var string
     */
    private $url;

    /** @var  string */
    private $path;

    /**
     * @var string
     */
    private $namePrefix = '';

    /**
     * @var string
     */
    private $nameSuffix = '';

    /**
     * @return float
     */
    public function getAspect()
    {
        return $this->aspect;
    }

    /**
     * @param float $aspect
     *
     * @return $this
     */
    public function setAspect($aspect)
    {
        $this->aspect = $aspect;

        return $this;
    }

    /**
     * @return int
     */
    public function getAudioBitRate()
    {
        return $this->audioBitRate;
    }

    /**
     * @param int $audioBitRate
     *
     * @return $this
     */
    public function setAudioBitRate($audioBitRate)
    {
        $this->audioBitRate = $audioBitRate;

        return $this;
    }

    /**
     * @return string
     */
    public function getAudioChannel()
    {
        return $this->audioChannel;
    }

    /**
     * @param string $audioChannel
     *
     * @return $this
     */
    public function setAudioChannel($audioChannel)
    {
        $this->audioChannel = $audioChannel;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param int $fileSize
     *
     * @return $this
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     *
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getNamePrefix()
    {
        return $this->namePrefix;
    }

    /**
     * @param string $namePrefix
     *
     * @return $this
     */
    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameSuffix()
    {
        return $this->nameSuffix;
    }

    /**
     * @param string $nameSuffix
     *
     * @return $this
     */
    public function setNameSuffix($nameSuffix)
    {
        $this->nameSuffix = $nameSuffix;

        return $this;
    }

    /**
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }

    /**
     * @param string $previewUrl
     *
     * @return $this
     */
    public function setPreviewUrl($previewUrl)
    {
        $this->previewUrl = $previewUrl;

        return $this;
    }

    /**
     * @return int
     */
    public function getResolution()
    {
        return $this->resolution;
    }

    /**
     * @param int $resolution
     *
     * @return $this
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getVideoBitRate()
    {
        return $this->videoBitRate;
    }

    /**
     * @param string $videoBitRate
     *
     * @return $this
     */
    public function setVideoBitRate($videoBitRate)
    {
        $this->videoBitRate = $videoBitRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

}
