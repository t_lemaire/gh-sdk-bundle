<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\NCMECQueryResult;

/**
 * This result is sent back to you in response to a NCMECVideoQueryOperation, if that operation succeeds.  You can
 * call getResponse() to get the exact message from NCMEC.  You can use the convenience methods to interpret the
 * response.
 */
class NCMECVideoQueryResult extends NCMECQueryResult
{

}
