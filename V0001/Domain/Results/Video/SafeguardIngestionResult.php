<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;

/**
 * This is sent to the client if an image or a video was successfully ingested into a Safeguard database.
 */
class SafeguardIngestionResult extends VideoResult
{
    /**
     * @var SafeguardIngestionMatch[]
     */
    private $matches = [];

    /** @var string The ID assigned by Safeguard to this ingestion. */
    private $safeguardIngestionToken;

    /**
     * @return SafeguardIngestionMatch[]
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * @param SafeguardIngestionMatch[] $matches
     * @return $this
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;
        return $this;
    }

    /**
     * @return string The ID assigned by Safeguard to this ingestion.
     */
    public function getSafeguardIngestionToken()
    {
        return $this->safeguardIngestionToken;
    }

    /**
     * @param string $safeguardIngestionToken The ID assigned by Safeguard to this ingestion.
     * @return $this
     */
    public function setSafeguardIngestionToken($safeguardIngestionToken)
    {
        $this->safeguardIngestionToken = $safeguardIngestionToken;
        return $this;
    }


}
