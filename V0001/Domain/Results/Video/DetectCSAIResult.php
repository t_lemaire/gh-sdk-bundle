<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;
use GorillaHub\SDKs\UploadBundle\V0001\Domain\CSAIMatchingClip;

/**
 * If a DetectCSAIOperation is successful, then this describes the result.  The result collects together the
 * results of one or more "matching jobs", which are jobs that run on Google's side to see if a video matches
 * something in their database.
 */
class DetectCSAIResult extends VideoResult
{
    /**
     * @var string[] The names of the matching jobs, according to Google.
     */
    private $names;

    /** @var CSAIMatchingClip[] Zero or more matching clips. */
    private $matchingClips;

    /**
     * @var string[] The raw responses from Google for the matching jobs.  The client must save these to its database
     *      for future reference even if (ESPECIALLY IF) the result was NOT a match.
     */
    private $rawResponses;

    /**
     * @return string[] @see $names
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string[] $names @see $names
     * @return $this
     */
    public function setNames($names)
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return CSAIMatchingClip[] @see $matchingClips
     */
    public function getMatchingClips()
    {
        return $this->matchingClips;
    }

    /**
     * @param CSAIMatchingClip[] $matchingClips @see $matchingClips
     * @return $this
     */
    public function setMatchingClips($matchingClips)
    {
        $this->matchingClips = $matchingClips;
        return $this;
    }

    /**
     * @return string[] @see $rawResponses
     */
    public function getRawResponses()
    {
        return $this->rawResponses;
    }

    /**
     * @param string[] $rawResponses @see $rawResponses
     * @return $this
     */
    public function setRawResponses($rawResponses)
    {
        $this->rawResponses = $rawResponses;
        return $this;
    }



    /** @return bool True if at least one match was found, false otherwise. */
    public function wasMatchFound() {
        return $this->matchingClips !== [];
    }
}
