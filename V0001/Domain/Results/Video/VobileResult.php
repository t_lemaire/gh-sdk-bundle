<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\UnexpectedVobileResponseException;
use GorillaHub\SDKs\SDKBundle\V0001\Helpers\Vobile\QueryResultResponse;

class VobileResult extends VideoResult
{

    /**
     * @var string
     */
    private $result;

    /**
     * @var string
     */
    private $crrResult;

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     *
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @return string
     */
    public function getCrrResult()
    {
        return $this->crrResult;
    }

    /**
     * @param string $crrResult
     */
    public function setCrrResult($crrResult)
    {
        $this->crrResult = $crrResult;
    }

    /**
     * Returns an object that helps you interpret the response from Vobile.
     *
     * @return QueryResultResponse
     * @throws UnexpectedVobileResponseException
     */
    public function getVobileCreateTaskResponse() {
        return new QueryResultResponse($this->result);
    }


}
