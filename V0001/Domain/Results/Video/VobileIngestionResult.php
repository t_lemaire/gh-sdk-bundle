<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;


/**
 * A result of this type is sent in a SuccessCall if the job included a VobileIngestionOperation and that
 * operation completed successfully.  Vobile returns a receipt XML that describes the new record in its
 * database.  You can call getReceipt() to retrieve the entire XML as it was sent by Vobile, or call
 * the individual methods for extracting information from the receipt.
 */
class VobileIngestionResult extends VideoResult
{

    /**
     * @var string The receipt from Vobile with information about the new database record.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Creating+a+job
     */
    private $receipt;

    /**
     * @return string The receipt from Vobile with information about the new database record.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Creating+a+job
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param string $receipt The receipt from Vobile with information about the new database record.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Creating+a+job
     *
     * @return $this
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;

        return $this;
    }



}
