<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\ImageBased\PatternRange;

class ImageResultParameter
{

    /**
     * @var string
     */
    private $format;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var float
     */
    private $aspect;

    /**
     * @var int
     */
    private $quality;

    /**
     * @var int
     */
    private $count;

    /**
     * @var PatternRange
     */
    private $patternRange;

    /**
     * @var string
     */
    private $patternUrl;

    /** @var  string */
    private $patternPath;

    /**
     * @var string
     */
    private $namePrefix = '';

    /**
     * @var string
     */
    private $nameSuffix = '';

    /**
     * @var string
     */
    private $resultId = '';

    /**
     * @return float
     */
    final public function getAspect()
    {
        return $this->aspect;
    }

    /**
     * @param float $aspect
     *
     * @return $this
     */
    final public function setAspect($aspect)
    {
        $this->aspect = $aspect;

        return $this;
    }

    /**
     * @return int
     */
    final public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return $this
     */
    final public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return string
     */
    final public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     *
     * @return $this
     */
    final public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return int
     */
    final public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     *
     * @return $this
     */
    final public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    final public function getNamePrefix()
    {
        return $this->namePrefix;
    }

    /**
     * @param string $namePrefix
     *
     * @return $this
     */
    final public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;

        return $this;
    }

    /**
     * @return string
     */
    final public function getNameSuffix()
    {
        return $this->nameSuffix;
    }

    /**
     * @param string $nameSuffix
     *
     * @return $this
     */
    final public function setNameSuffix($nameSuffix)
    {
        $this->nameSuffix = $nameSuffix;

        return $this;
    }

    /**
     * @return PatternRange
     */
    final public function getPatternRange()
    {
        return $this->patternRange;
    }

    /**
     * @param PatternRange $patterRange
     *
     * @return $this
     */
    final public function setPatternRange(PatternRange $patterRange)
    {
        $this->patternRange = $patterRange;

        return $this;
    }

    /**
     * @return string
     */
    final public function getPatternUrl()
    {
        return $this->patternUrl;
    }

    /**
     * @param string $patternUrl
     *
     * @return $this
     */
    final public function setPatternUrl($patternUrl)
    {
        $this->patternUrl = $patternUrl;

        return $this;
    }

    /**
     * @return int
     */
    final public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     *
     * @return $this
     */
    final public function setQuality($quality)
    {
        $this->quality = $quality;

        return $this;
    }

    /**
     * @return int
     */
    final public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    final public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultId()
    {
        return $this->resultId;
    }

    /**
     * @param string $resultId
     *
     * @return $this
     */
    public function setResultId($resultId)
    {
        $this->resultId = $resultId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatternPath()
    {
        return $this->patternPath;
    }

    /**
     * @param string $patternPath
     */
    public function setPatternPath($patternPath)
    {
        $this->patternPath = $patternPath;
    }

} 