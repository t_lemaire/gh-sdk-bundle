<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters;

class OriginalInfoParameter
{

    /**
     * @var string
     */
    private $format;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $resolution;

    /**
     * @var float
     */
    private $aspect;

    /**
     * @var string
     */
    private $videoCodec;

    /**
     * @var int
     */
    private $videoBitRate;

    /**
     * @var string
     */
    private $audioCodec;

    /**
     * @var string
     */
    private $audioBitRate;

    /**
     * @var string
     */
    private $audioSampleRate;

    /**
     * @var string
     */
    private $audioChannel;

    /**
     * @var float
     */
    private $rotation;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * @var string
     */
    private $fileExtension;

    /**
     * @return float
     */
    public function getAspect()
    {
        return $this->aspect;
    }

    /**
     * @param float $aspect
     */
    public function setAspect($aspect)
    {
        $this->aspect = $aspect;
    }

    /**
     * @return string
     */
    public function getAudioBitRate()
    {
        return $this->audioBitRate;
    }

    /**
     * @param string $audioBitRate
     */
    public function setAudioBitRate($audioBitRate)
    {
        $this->audioBitRate = $audioBitRate;
    }

    /**
     * @return string
     */
    public function getAudioChannel()
    {
        return $this->audioChannel;
    }

    /**
     * @param string $audioChannel
     */
    public function setAudioChannel($audioChannel)
    {
        $this->audioChannel = $audioChannel;
    }

    /**
     * @return string
     */
    public function getAudioCodec()
    {
        return $this->audioCodec;
    }

    /**
     * @param string $audioCodec
     */
    public function setAudioCodec($audioCodec)
    {
        $this->audioCodec = $audioCodec;
    }

    /**
     * @return string
     */
    public function getAudioSampleRate()
    {
        return $this->audioSampleRate;
    }

    /**
     * @param string $audioSampleRate
     */
    public function setAudioSampleRate($audioSampleRate)
    {
        $this->audioSampleRate = $audioSampleRate;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * @param string $fileExtension
     */
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * @param int $fileSize
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getResolution()
    {
        return $this->resolution;
    }

    /**
     * @param int $resolution
     */
    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * @return float
     */
    public function getRotation()
    {
        return $this->rotation;
    }

    /**
     * @param float $rotation
     */
    public function setRotation($rotation)
    {
        $this->rotation = $rotation;
    }

    /**
     * @return int
     */
    public function getVideoBitRate()
    {
        return $this->videoBitRate;
    }

    /**
     * @param int $videoBitRate
     */
    public function setVideoBitRate($videoBitRate)
    {
        $this->videoBitRate = $videoBitRate;
    }

    /**
     * @return string
     */
    public function getVideoCodec()
    {
        return $this->videoCodec;
    }

    /**
     * @param string $videoCodec
     */
    public function setVideoCodec($videoCodec)
    {
        $this->videoCodec = $videoCodec;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

}