<?php
/**
 * User: Fabrice Baumann - capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 02/09/16 - 4:21 PM
 */

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;


class StoreInTheOriginResult extends Result
{
    private $path = "";

    /**
     * Setter method that will set the variable $path to the passed parameter.
     *
     * @param string $path
     */
    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * getter method that will return the variable of variable $path.
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }
}