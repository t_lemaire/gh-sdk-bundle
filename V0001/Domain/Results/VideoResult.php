<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\OriginalInfoParameter;

abstract class VideoResult extends Result
{

    /**
     * @var OriginalInfoParameter
     */
    protected $originalInfo;

    /**
     * @return OriginalInfoParameter
     */
    public function getOriginalInfo()
    {
        return $this->originalInfo;
    }

    /**
     * @param OriginalInfoParameter $originalInfo
     *
     * @return $this
     */
    public function setOriginalInfo(OriginalInfoParameter $originalInfo)
    {
        $this->originalInfo = $originalInfo;

        return $this;
    }


}
