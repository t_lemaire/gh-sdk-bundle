<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

class ErrorResult extends Result
{
    /**
     * @var string
     */
    private $message = '';

    /**
     * @var string
     */
    private $metadata = '';

    /**
     * @return string
     */
    final public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    final public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    final public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     *
     * @return $this
     */
    final public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

}