<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\ImageResultParameter;

class ImageBasedResult extends VideoResult
{

    private $imageResults = array();

    /**
     * @param ImageResultParameter $result
     *
     * @return $this
     */
    public function addImageResult(ImageResultParameter $result)
    {
        $this->imageResults[$result->getResultId()] = $result;

        return $this;
    }

    /**
     * @return ImageResultParameter[]
     */
    public function getImageResults()
    {
        return $this->imageResults;
    }

    /**
     * @param ImageResultParameter[] $imageResults
     *
     * @return $this
     */
    public function setImageResults(array $imageResults = array())
    {
        $this->imageResults = array();

        foreach ($imageResults as $result) {
            if ($result instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\ImageResultParameter) {
                $this->imageResults[$result->getResultId()] = $result;
            }
        }

        return $this;
    }

}
