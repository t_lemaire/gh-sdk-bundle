<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * An instance of this class is used to interpret the result of a specific pipeline.  Call the appropriate
 * method on AgeEstimationResult to get an instance of this class.
 */
abstract class AgeEstimationPipelineResult extends Result
{
    /**
     * @var object The (JSON-decoded) result from the age estimation service for a particular pipeline; for example,
     *      this could be the value of the "face" field in the result.
     */
    protected $result;

    /**
     * @param object $result The (JSON-decoded) result from the age estimation service for a particular pipeline; for
     *      example, this could be the value of the "face" field in the result.
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * @return string|null A version number, like "v0.1", or null if none was specified.
     */
    public function getVersion() {
        return isset($this->result->version) ? ((string)$this->result->version) : null;
    }

    /**
     * @return float|null The number of seconds taken to produce these results, or null if not specified.
     */
    public function getTime() {
        return isset($this->result->time) ? ((float)$this->result->time) : null;
    }


}
