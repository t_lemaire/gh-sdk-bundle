<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * This is used to interpret the result of the "face_age" pipeline.
 */
class AgeEstimationFaceAgeResult extends AgeEstimationPipelineResult
{
    /**
     * @return int|null The number of under 25 faces detected, or null if not specified.
     */
    public function getNumUnder25Faces() {
        if (isset($this->result->results->groups_under25)) {
            return (int)$this->result->groups_under25;
        }
        if (isset($this->result->results->younger) && is_array($this->result->results->younger)) {
            return count($this->result->results->younger);
        }
        return null;
    }

    /**
     * @return float|null The mean age of those under 25, or null if unknown.
     */
    public function getUnder25MeanAge() {
        return isset($this->result->results->younger_mean_age) ? ((float)$this->result->younger_mean_age) : null;
    }

    /**
     * @return AgeEstimationFaceGroup[] Groups of faces that seem to be under 25 years old.
     */
    public function getUnder25Groups() {
        $groups = [];
        if ($this->result->results->younger && is_array($this->result->results->younger)) {
            foreach ($this->result->results->younger as $younger) {
                $groups[] = new AgeEstimationFaceGroup($younger);
            }
        }
        return $groups;
    }

    /**
     * @return int|null The number of over 25 faces detected, or null if not specified.
     */
    public function getNumOver25Faces() {
        if (isset($this->result->results->groups_over25)) {
            return (int)$this->result->groups_over25;
        }
        if (isset($this->result->results->older) && is_array($this->result->results->older)) {
            return count($this->result->results->older);
        }
        return null;
    }

    /**
     * @return float|null The mean age of those over 25, or null if unknown.
     */
    public function getOver25MeanAge() {
        return isset($this->result->results->older_mean_age) ? ((float)$this->result->older_mean_age) : null;
    }

    /**
     * @return AgeEstimationFaceGroup[] Groups of faces that seem to be over 25 years old.
     */
    public function getOver25Groups() {
        $groups = [];
        if ($this->result->results->older && is_array($this->result->results->older)) {
            foreach ($this->result->results->older as $older) {
                $groups[] = new AgeEstimationFaceGroup($older);
            }
        }
        return $groups;
    }

    /**
     * @return int|null The number of faces that were too small to assess, or null if not specified.
     */
    public function getNumFacesTooSmall() {
        return isset($this->result->results->faces_too_small) ? ((int)$this->result->results->faces_too_small) : null;
    }
}
