<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * A particular face that was detected on a particular frame by "face_azure".
 */
class AgeEstimationFaceAzureDetection
{
    /**
     * @var object $detectResult The detection result from the Age Estimation service, including the "face",
     *      "is_focus", etc.. fields.
     */
    private $detectResult;

    /**
     * @param object $detectResult The detection result from the Age Estimation service, including the "face",
     *      "is_focus", etc.. fields.
     */
    public function __construct($detectResult) {
        $this->detectResult = $detectResult;
    }


    /**
     * @return int|null A number identifying the face, or null if not specified.  All faces with the same ID seem
     *      to be the same person.
     */
    public function getFaceId() {
        return isset($this->detectResult->face) ? ((int)$this->detectResult->face) : null;
    }

    /**
     * @return bool|null True if the face seems to be in the foreground of the shot.
     */
    public function getIsFocus() {
        return isset($this->detectResult->is_focus) ? ((bool)$this->detectResult->is_focus) : null;
    }

    /**
     * @return float|null The confidence, from 0 to 1, that this is actually a face.
     */
    public function getDetectScore() {
        return isset($this->detectResult->detect_score) ? ((float)$this->detectResult->detect_score) : null;
    }

    /**
     * @return float|null The confidence, from 0 to 1, in the age score, or null if no age was detected.
     */
    public function getAgeScore() {
        return isset($this->detectResult->age_score) ? ((float)$this->detectResult->age_score) : null;
    }


    /**
     * @return int|null The guessed age, or null if it could not be determined.
     */
    public function getAge() {
        return isset($this->detectResult->age) ? ((int)$this->detectResult->age) : null;
    }
}