<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * This is used to interpret one frame in the result of the "face_azure" pipeline.
 */
class AgeEstimationFaceAzureFrame
{
    /**
     * @var object Result relating to a particular frame, containing "face", "is_focus", etc...
     */
    private $frameResult;

    /**
     * @param object $frameResult Result relating to a particular frame, containing "frame", "detects", etc...
     */
    public function __construct($frameResult) {
        $this->frameResult = $frameResult;
    }

    /**
     * @return int|null The number of the frame, or null if not specified.
     */
    public function getFrame() {
        return isset($this->frameResult->frame) ? ((int)$this->frameResult->frame) : null;
    }

    /**
     * @return string|null The URL of the image that shows the face detection, or null if none.  If you called
     *      setAzureFilePattern() on the AgeEstimationOperation to specify where to store the images, then the
     *      images will be stored on your Isilon and the URL will be of the form "file://volume/path/file.ext",
     *      where "volume" is the name of the volume as known to the Origin Pull service (e.g. "c3605").
     *      Otherwise, these are URLs provided by the age estimation service that point to temporary files that
     *      will be deleted in approximately 5 days.
     */
    public function getImageUrl() {
        return isset($this->frameResult->presigned_url) ? ((string)$this->frameResult->presigned_url) : null;
    }


    /**
     * @return AgeEstimationFaceAzureDetection[]
     */
    public function getDetections() {
        $detections = [];
        if (isset($this->frameResult->detects) && is_array($this->frameResult->detects)) {
            foreach ($this->frameResult->detects as $detect) {
                $detections[] = new AgeEstimationFaceAzureDetection($detect);
            }
        }
        return $detections;
    }
}
