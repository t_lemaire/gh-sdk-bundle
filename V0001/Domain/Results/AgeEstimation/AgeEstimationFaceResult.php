<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * This is used to interpret the result of the "face" pipeline.
 */
class AgeEstimationFaceResult extends AgeEstimationPipelineResult
{
    /**
     * @return int|null The total number of faces detected, or null if not specified.
     */
    public function getNumFaceDetected() {
        return isset($this->result->num_face_detected) ? ((int)$this->result->num_face_detected) : null;
    }

    /**
     * @return int The number of video frames that were looked at, or null if not specified.  This is not specified
     *      if the original media was an image.
     */
    public function getNumFramesLooked() {
        return isset($this->result->num_frames_looked) ? ((int)$this->result->num_frames_looked) : null;
    }
}
