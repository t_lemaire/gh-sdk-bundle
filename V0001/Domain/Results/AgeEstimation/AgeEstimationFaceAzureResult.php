<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * This is used to interpret the result of the "face_azure" pipeline.
 */
class AgeEstimationFaceAzureResult extends AgeEstimationPipelineResult
{
    /**
     * @return int|null The number of azure calls, or null if not specified.
     */
    public function getNumAzureCalls() {
        return isset($this->result->azure_calls) ? ((int)$this->result->azure_calls) : null;
    }

    /**
     * @return int|null The number of azure detections, or null if not specified.
     */
    public function getNumAzureDetections() {
        return isset($this->result->azure_detections) ? ((int)$this->result->azure_detections) : null;
    }

    /**
     * @return int|null The number of azure errors, or null if not specified.
     */
    public function getNumAzureErrors() {
        return isset($this->result->azure_errors) ? ((int)$this->result->azure_errors) : null;
    }
}
