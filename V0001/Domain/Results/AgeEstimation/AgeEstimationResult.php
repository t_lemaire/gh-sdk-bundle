<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * This result is sent back to you in response to an AgeEstimationOperation, if that operation succeeds.  You can
 * call getResponse() to get the exact message from the service.  You can use the convenience methods to interpret the
 * response.
 */
class AgeEstimationResult extends Result 
{
    /** @var object The (JSON-decoded) response from the age estimation service. */
    private $response;

    /**
     * @return object The (JSON-decoded) response from the age estimation service.
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param object $response The (JSON-decoded) response from the age estimation service.
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @return AgeEstimationFaceResult|null The result of the "face" pipeline, if it was present.
     */
    public function getFaceResult() {
        return isset($this->response->face) ? new AgeEstimationFaceResult($this->response->face) : null;
    }

    /**
     * @return AgeEstimationFaceAgeResult|null The result of the "face_age" pipeline, if it was present.
     */
    public function getFaceAgeResult() {
        return isset($this->response->face_age) ? new AgeEstimationFaceAgeResult($this->response->face_age) : null;
    }

    /**
     * @return AgeEstimationFaceAzureResult|null The result of the "face_azure" pipeline, if it was present.
     */
    public function getFaceAzureResult() {
        return isset($this->response->face_azure)
                ? new AgeEstimationFaceAzureResult($this->response->face_azure)
                : null;
    }

}
