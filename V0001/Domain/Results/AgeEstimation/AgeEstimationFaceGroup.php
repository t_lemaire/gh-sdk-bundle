<?php
namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\AgeEstimation;

/**
 * This is used to interpret a "face group".
 */
class AgeEstimationFaceGroup
{
    /**
     * @var object The result for a particular group, containing the fields "is_under25_mean", "best_frame", etc...
     */
    private $groupResult;

    /**
     * @param object $groupResult The result for a particular group, containing the fields "is_under25_mean",
     *      "best_frame", etc...
     */
    public function __construct($groupResult) {
        $this->groupResult = $groupResult;
    }

    /**
     * @return float|null
     */
    public function getIsUnder25Mean() {
        return isset($this->groupResult->is_under25_mean) ? ((float)$this->groupResult->is_under25_mean) : null;
    }

    /**
     * @return int|null
     */
    public function getBestFrame() {
        return isset($this->groupResult->best_frame) ? ((int)$this->groupResult->best_frame) : null;
    }

    /**
     * @return int|null
     */
    public function getBestIndex() {
        return isset($this->groupResult->best_idx) ? ((int)$this->groupResult->best_idx) : null;
    }

    /**
     * @return int|null
     */
    public function getNumFrames() {
        return isset($this->groupResult->num_frames) ? ((int)$this->groupResult->num_frames) : null;
    }

}
