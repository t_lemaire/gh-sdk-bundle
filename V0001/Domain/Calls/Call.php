<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\ResultInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

abstract class Call implements SDKCallInterface
{
    /**
     * @var SDKCallInterface
     */
    protected $originalCall;
    /**
     * @var ResultInterface
     */
    protected $result;
    /**
     * @var Signature
     */
    private $signature;

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    final public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    final public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface
     */
    final public function getOriginalCall()
    {
        return $this->originalCall;
    }

    /**
     * @param \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface $originalCall
     *
     * @return $this
     */
    final public function setOriginalCall(SDKCallInterface $originalCall)
    {
        $this->originalCall = $originalCall;

        return $this;
    }

    /**
     * @return \GorillaHub\SDKs\SDKBundle\V0001\Domain\ResultInterface
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param \GorillaHub\SDKs\SDKBundle\V0001\Domain\ResultInterface $result
     *
     * @return $this
     */
    public function setResult(ResultInterface $result)
    {
        $this->result = $result;

        return $this;
    }

}
