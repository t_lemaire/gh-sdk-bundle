<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

class DateTime implements DateTimeInterface
{
    /**
     * @var string
     */
    private $value;

    function __construct()
    {
        $dateTime = new \DateTime('now');
        $dateTime->setTimezone(new \DateTimeZone('UTC'));

        $this->value = $dateTime->format(DATE_ISO8601);
    }

    /**
     * @param string $datetime
     *
     * @return $this
     */
    public function setISO8601($datetime)
    {
        $this->value = $datetime;

        return $this;
    }

    /**
     * @return int
     */
    public function getISO8601()
    {
        return $this->value;
    }

    /**
     * @return \DateTime
     */
    public function toDateTime()
    {
        return new \DateTime($this->value);
    }

}