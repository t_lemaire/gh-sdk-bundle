<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Class Signature
 * @package GorillaHub\SDKs\SDKBundle\V0001\Domain
 */
class Signature
{

    /**
     * @var string
     */
    private $jobId;

    /**
     * @var int
     */
    private $clientId;

    /**
     * @var string
     */
    private $transactionId = '';

    /**
     * This sets a newly generated job ID.
     */
    final public function generateAndSetNewJobId()
    {
        $this->jobId = $this->generateHexString(32);
    }

    /**
     * @param int $numberOfChars
     * @return string
     */
    private function generateHexString($numberOfChars)
    {
        if (function_exists('random_bytes')) {
            $job = random_bytes((int)ceil($numberOfChars / 2));
        } else {
            $job = '';
            $numberOfWords = ceil($numberOfChars / 4);
            $time = time();	    
            for ($i = 0; $i < $numberOfWords; $i++) {
                $job .= pack('S', mt_rand(0, 0xFFFF) ^ ($time & 0xFFFF) ^ (($time >> 16) & 0xFFFF));
            }
        }
        return substr(strtolower(bin2hex($job)), 0, $numberOfChars);

    }

    /**
     * This sets a newly generated transaction ID.
     */
    final public function generateAndSetNewTransactionId()
    {
        $this->transactionId = $this->generateHexString(32);
    }

    /**
     * Returns the transaction id.
     *
     * @return string
     */
    final public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Sets the transaction id.
     *
     * @param string $id
     *
     * @return $this
     */
    final public function setTransactionId($id = '')
    {
        $this->transactionId = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'jobId:' . $this->getJobId() . ', clientId' . $this->getClientId();
    }

    /**
     * Returns the job id.
     *
     * @return string
     */
    final public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * Sets the job Id,
     * Jod Id has to be a string of 32 characters long, containing characters form 0-9 and lowercase letters from a-f only.
     *
     * @param string $id
     *
     * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @return $this
     */
    final public function setJobId($id)
    {
        if (!is_string($id)) {
            throw new InvalidParameterException('Job Id has to be a string value.');
        }

        if (strlen($id) !== 32) {
            throw new InvalidParameterException('Job Id has to be 32 characters long.');
        }

        if (0 == preg_match('/^[0-9a-f]{32}$/', $id)) {
            throw new InvalidParameterException('Invalid Job Id, it must contain any combination of numbers from 0-9 and lower case letters from a-f only.');
        }

        $this->jobId = $id;

        return $this;
    }

    /**
     * Returns the client id.
     *
     * @return int
     */
    final public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the client id.
     *
     * @param int $id
     *
     * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @return $this
     */
    final public function setClientId($id)
    {
        $id = (int)$id;

        if ($id <= 0) {
            throw new InvalidParameterException('Client Id is not valid.');
        }

        $this->clientId = $id;

        return $this;
    }

}