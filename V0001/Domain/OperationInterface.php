<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain;

/**
 * Interface OperationInterface
 *
 * @package GorillaHub\SDKs\EncodeBundle\V0001\Domain
 */
interface OperationInterface
{
    const STATUS_FAILED = 'failed';
    const STATUS_SUCCESS = 'success';
    const STATUS_IN_PROGRESS = 'in_progress';

    /**
     * Set the operation id
     *
     * @param string $id
     * @return self
     */
    public function setOperationId($id);

    /**
     * Return the operation id
     *
     * @return string
     */
    public function getOperationId();

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setCustomId($id);

    /**
     * @return string
     */
    public function getCustomId();

    /**
     * Return the current status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set the status
     *
     * @param string $status
     * @return self
     */
    public function setStatus($status);

    /**
     * Set the job priority
     *
     * @param int $priority
     * @return self
     */
    public function setPriority($priority);

    /**
     * Return the job priority
     *
     * @return mixed
     */
    public function getPriority();

}