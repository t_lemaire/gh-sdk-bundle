<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats;

class ImageFormats
{
    const FORMAT_JPG = 'jpg';
    const FORMAT_PNG = 'png';
    const FORMAT_GIF = 'gif';
}