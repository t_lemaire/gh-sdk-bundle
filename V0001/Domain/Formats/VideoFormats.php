<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats;

class VideoFormats
{
    const FORMAT_FLV = 'flv';
    const FORMAT_MP4 = 'mp4';
    const FORMAT_3GP = '3gp';
    const FORMAT_WEBM = 'webm';
    const FORMAT_OGG = 'ogg';
    const FORMAT_MPEG_4 = 'MPEG-4';
}