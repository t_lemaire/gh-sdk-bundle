<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Services;

use GorillaHub\FilesBundle\Paths;

/**
 * Class FilePatternParser
 * @package GorillaHub\SDKs\SDKBundle\V0001\Services
 */
class FilePatternParser
{
    const YEAR = '%y';
    const MONTH = '%m';
    const DAY = '%d';
    const FILE_NAME = '%f';
    const FILE_EXTENSION = '%x';
    const ITERATOR = '%i';

    /**
     * @var array
     */
    private $definitions = array();

    /**
     * @return array
     */
    public function getDefinitions()
    {
        return $this->definitions;
    }

    /**
     * @param array $definitions
     *
     * @return $this
     */
    public function setDefinitions(array $definitions = array())
    {
        $this->definitions = $definitions;

        return $this;
    }

    /**
     * @param string $pattern
     * @param null $patternDefinitions
     * @return string
     */
    public function parse($pattern, $patternDefinitions = null)
    {
        $definitions = $this->definitions;
        if (!empty($patternDefinitions)) {
            $definitions = $patternDefinitions;
        }

        foreach ($definitions as $key => $value) {
            if (is_object($key) || is_object($value) || is_array($key) || is_array($value)) {
                continue;
            }

            $pattern = mb_ereg_replace('(' . $key . ')', $value, $pattern);
        }

        $pattern = str_replace('\\\\', '\\', $pattern);

        return Paths::cleanPath($pattern);
    }

    /**
     * This function will take the initialValue set and add (replace) year, month, and day values from the DateTime object with corresponding place holders as keys
     *
     * @param \DateTime $date
     * @param array $values --- original set of values, the function result will be added on top of these
     * @return array
     */
    public function buildValuesForDate(\DateTime $date, array $values = [])
    {
        $values[FilePatternParser::YEAR] = $date->format('Y');
        $values[FilePatternParser::MONTH] = $date->format('m');
        $values[FilePatternParser::DAY] = $date->format('d');
        return $values;
    }

    /**
     * This function will take the initialValue set and add (replace) filename an file extension values from the DateTime object with corresponding place holders as keys
     *
     * @param string $path
     * @param array $values --- original set of values, the function result will be added on top of these
     * @return array
     */
    public function buildValuesForPath($path, array $values = [])
    {
        $pathComponents = pathinfo($path);
        $values[FilePatternParser::FILE_NAME] = $pathComponents['filename'];
        $values[FilePatternParser::FILE_EXTENSION] = $pathComponents['extension'];
        return $values;
    }
}