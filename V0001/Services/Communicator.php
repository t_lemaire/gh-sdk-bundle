<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Services;

use GorillaHub\CurlBundle\Connection as CurlConnection;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\CallEventRecords\HttpCallEventRecord;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Responses\Response;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\ExceptionTransmitterException;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\InvalidResponseException;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException;
use GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter;
use GorillaHub\SDKs\SDKBundle\V0001\Helpers\JsonWebSignature;

class Communicator
{

    const RESPONSE_EXCEPTION = 'exception';
    const RESPONSE_OK = 'ok';

    protected $targetUrl;

    /**
     * @var \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface
     */
    protected $call;

    /**
     * @var string
     */
    protected $rawResponse = '';

    /**
     * @var \GorillaHub\CurlBundle\Connection
     */
    protected $curl;

    /**
     * @var Response
     */
    protected $responseObj;

    /**
     * @var \GorillaHub\JSONSerializerBundle\JSONSerializer
     */
    protected $serializer;

    /**
     * @var HttpCallEventRecord A record of the most recent call.
     */
    private $lastCallEventRecord = null;


    /**
     * Sends the job to the API.
     *
     * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     */
    public function send()
    {
        $this->lastCallEventRecord = new HttpCallEventRecord();
        $this->lastCallEventRecord->setUrl($this->targetUrl);

        if (null === $this->call) {
            throw new NotFullyDefinedException('Call must be populated before being sent.');
        }

        $signature = $this->call->getSignature();

        if (!($signature instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature)) {
            throw new NotFullyDefinedException('Call must contain a signature before being sent.');
        }

        if (null === $this->serializer) {
            throw new NotFullyDefinedException('Serializer must be populated.');
        }

        if (null === $this->curl) {
            throw new NotFullyDefinedException('Curl must be populated.');
        }

        $rawRequest = 'job=' . urlencode($this->serializer->serialize($this->call));
        $this->lastCallEventRecord->setRawRequest($rawRequest);

        try {
            $rawResponse = $this->curl->post($this->targetUrl, $rawRequest);
            $this->lastCallEventRecord->setHttpStatus($this->curl->getHttpStatus());
            $this->lastCallEventRecord->setRawResponse($rawResponse);
            $this->curl->setOption(CURLOPT_HTTPHEADER, []);
            $this->parseResponse($rawResponse);
        } catch (\Exception $e) {
            $this->lastCallEventRecord->setDurationSeconds();
            throw $e;
        }
        $this->lastCallEventRecord->setWasSuccessful(true);
        $this->lastCallEventRecord->setDurationSeconds();
    }

    /**
     * Parses the response from the API call.
     *
     * @param string $response
     *
     * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\InvalidResponseException
     * @throws \Exception
     */
    protected function parseResponse($response)
    {
        $this->rawResponse = $response;

        if (false === $response) {
            throw new InvalidResponseException('An error happened during the request. Error:' . $this->curl->getError());
        }

        try {
            $this->responseObj = $this->serializer->deserialize($response);
        } catch (\Exception $e) {
            throw new InvalidResponseException('Invalid response format.');
        }

        if ($this->responseObj instanceof ExceptionTransmitter) {
            $exception = new ExceptionTransmitterException($this->responseObj->getMessage(),
                $this->responseObj->getCode());
            $exception->setType($this->responseObj->getType());
            throw $exception;
        }
    }

    /**
     * Returns the raw response from the API call.
     *
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * Returns the response object.
     *
     * @return Response
     */
    public function getResponseObject()
    {
        return $this->responseObj;
    }

    /**
     * Returns the used serializer object.
     *
     * @return \GorillaHub\JSONSerializerBundle\JSONSerializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @param \GorillaHub\JSONSerializerBundle\JSONSerializer $serializer
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Returns the used curl connection object.
     * Useful for debugging.
     *
     * @return CurlConnection
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param \GorillaHub\CurlBundle\Connection $curl
     */
    public function setCurl($curl)
    {
        $this->curl = $curl;
    }

    /**
     * Returns the current assigned job.
     *
     * @return \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface
     */
    public function getCall()
    {
        return $this->call;
    }

    /**
     * Sets the job to be sent.
     *
     * @param \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface $call
     */
    public function setCall(SDKCallInterface $call)
    {
        $this->call = $call;
    }

    /**
     * Returns the target API targetUrl.
     *
     * @return mixed
     */
    public function getTargetUrl()
    {
        return $this->targetUrl;
    }

    /**
     * Sets the target API url.
     *
     * @param string $targetUrl
     */
    public function setTargetUrl($targetUrl)
    {
        $this->targetUrl = $targetUrl;
    }

    /**
     * @return HttpCallEventRecord {@see $lastCallEventRecord}
     */
    public function getLastCallEventRecord()
    {
        return $this->lastCallEventRecord;
    }

    /**
     * @param string $key A key shared between the caller and receiver.
     * @param int $ttl The number of seconds after which the signature should expire.
     */
    public function setJWSAuthHeader($key, $ttl = 60) {
        $headerStrings = [];
        foreach (JsonWebSignature::getAuthenticationHeaders($key, $ttl) as $name => $value) {
            $headerStrings[] = $name . ': ' . $value;
        }
        $this->getCurl()->setOption(CURLOPT_HTTPHEADER, $headerStrings);
    }
}
