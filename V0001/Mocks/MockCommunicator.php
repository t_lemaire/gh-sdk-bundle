<?php

namespace GorillaHub\SDKs\SDKBundle\V0001\Mocks;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Responses\Response;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException;
use GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator;

class MockCommunicator extends Communicator
{
    /** @var Response */
    private $mockResponse = null;

    /**
     * Sends the job to the API
     *
     * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     */
    public function send()
    {
        if (null === $this->call) {
            throw new NotFullyDefinedException('Call must be populated before being sent.');
        }

        $signature = $this->call->getSignature();

        if (!($signature instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature)) {
            throw new NotFullyDefinedException('Call must contain a signature before being sent.');
        }

        $this->responseObj = $this->mockResponse;
    }

    /**
     * Sets the response that should be returned by getResponseObject.
     *
     * @param mixed $response
     */
    public function setMockResponseObject($response)
    {
        $this->mockResponse = $response;
    }
}
