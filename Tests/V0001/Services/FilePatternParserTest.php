<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Services\FilePatternParser;

class FilePatternParserTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingDefinitions()
    {
        $definitions = array(
            '%d' => 23,
            '%m' => 12
        );

        $parser = new \GorillaHub\SDKs\SDKBundle\V0001\Services\FilePatternParser();
        $parser->setDefinitions($definitions);
        $this->assertEquals($definitions, $parser->getDefinitions());
    }

    public function testParsingValues()
    {
        $pattern = '\%y\%m\%d\%f.%x';
        $expected = '\3\2\1\pocoyo.flv';

        $definitions = array(
            FilePatternParser::DAY => 1,
            FilePatternParser::MONTH => 2,
            FilePatternParser::YEAR => 3,
            FilePatternParser::FILE_NAME => 'pocoyo',
            FilePatternParser::FILE_EXTENSION => 'flv'
        );

        $parser = new FilePatternParser();
        $parser->setDefinitions($definitions);
        $this->assertEquals($expected, $parser->parse($pattern));
    }

    public function testParsingArrayValues()
    {
        $pattern = '\%y\%m\%d\%f.%x';
        $expected = '\%y\%m\%d\%f.%x';

        $definitions = array(
            FilePatternParser::DAY => array(1 => 1),
            FilePatternParser::MONTH => array(2 => 2),
            FilePatternParser::YEAR => array(3 => 3),
            FilePatternParser::FILE_NAME => array('p' => 'pocoyo'),
            FilePatternParser::FILE_EXTENSION => array('x' => 'flv')
        );

        $parser = new FilePatternParser();
        $parser->setDefinitions($definitions);
        $this->assertEquals($expected, $parser->parse($pattern));
    }

}
