<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\JSONSerializerBundle\JSONSerializer;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Preview;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\ExceptionTransmitterException;
use GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter;

class CommunicatorTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingCurlValue()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $curl = new \GorillaHub\CurlBundle\Connection();

        $communicator->setCurl($curl);
        $this->assertEquals($curl, $communicator->getCurl());
    }

    public function testSettingSerializerValue()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();

        $communicator->setSerializer($serializer);
        $this->assertEquals($serializer, $communicator->getSerializer());
    }

    public function testSettingJobValue()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();

        $communicator->setCall($job);
        $this->assertEquals($job, $communicator->getCall());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException
     * @expectedExceptionMessage Call must be populated before being sent.
     */
    public function testSendingWithNoCallException()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $communicator->send();
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException
     * @expectedExceptionMessage Call must contain a signature before being sent.
     */
    public function testSendingWithNoCallSignatureException()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();

        $communicator->setCall($job);

        $communicator->send();
    }


    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException
     * @expectedExceptionMessage Serializer must be populated.
     */
    public function testSendingWithNoSerializerException()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);

        $communicator->setCall($job);

        $communicator->send();
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\NotFullyDefinedException
     * @expectedExceptionMessage Curl must be populated.
     */
    public function testSendingWithNoCurlException()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);

        $serializer = new JSONSerializer();

        $communicator->setCall($job);
        $communicator->setSerializer($serializer);

        $communicator->send();
    }

    public function testSettingTargetUrlValue()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $url = 'http://www.youporn.com';

        $communicator->setTargetUrl($url);
        $this->assertEquals($url, $communicator->getTargetUrl());
    }

    public function testSendAction()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $url = 'http://www.youporn.com';
        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);
        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();
        $curl = new CurlMock();

        $communicator->setTargetUrl($url);
        $communicator->setCall($job);
        $communicator->setSerializer($serializer);
        $communicator->setCurl($curl);
        $communicator->send();

        $this->assertEquals('response', $communicator->getRawResponse());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\InvalidResponseException
     * @expectedExceptionMessage An error happened during the request.
     */
    public function testAFalseValueReceivedResponse()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $url = 'http://www.youporn.com';
        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);
        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();
        $curl = new CurlMock_ResponseValue_False();

        $communicator->setTargetUrl($url);
        $communicator->setCall($job);
        $communicator->setSerializer($serializer);
        $communicator->setCurl($curl);
        $communicator->send();
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\Communicator\InvalidResponseException
     * @expectedExceptionMessage Invalid response format.
     */
    public function testAnInvalidJsonValueReceivedResponse()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $url = 'http://www.youporn.com';
        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);
        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();
        $curl = new CurlMock_ResponseValue_InvalidJson();

        $communicator->setTargetUrl($url);
        $communicator->setCall($job);
        $communicator->setSerializer($serializer);
        $communicator->setCurl($curl);
        $communicator->send();
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Exception transmitter test message.
     */
    public function testAnExceptionTransmitterReceivedResponse()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $url = 'http://www.youporn.com';
        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);
        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();
        $curl = new CurlMock_ResponseValue_ExceptionTransmitter();

        $communicator->setTargetUrl($url);
        $communicator->setCall($job);
        $communicator->setSerializer($serializer);
        $communicator->setCurl($curl);
        $communicator->send();
    }

    function testExceptionTransmitterExceptionGetType()
    {
        $exception = new ExceptionTransmitterException();
        $exception->setType('exception');
        $result = $exception->getType();
        $this->assertEquals('exception', $result);
    }

    public function testGettingAResponseObject()
    {
        $communicator = new \GorillaHub\SDKs\SDKBundle\V0001\Services\Communicator();

        $signature = new Signature();

        $url = 'http://www.youporn.com';
        $job = new \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob();
        $job->setSignature($signature);
        $serializer = new \GorillaHub\JSONSerializerBundle\JSONSerializer();
        $curl = new CurlMock_ResponseValue();

        $communicator->setTargetUrl($url);
        $communicator->setCall($job);
        $communicator->setSerializer($serializer);
        $communicator->setCurl($curl);
        $communicator->send();

        $preview = new Preview();
        $preview->setDuration(10);
        $preview->setDefaultDuration(20);
        $preview->setStartTime(5);

        $this->assertEquals($preview, $communicator->getResponseObject());
    }

}


class CurlMock extends \GorillaHub\CurlBundle\Connection
{
    public function post($url, $data, array $options = null)
    {
        return 'response';
    }
}

class CurlMock_ResponseValue_False extends \GorillaHub\CurlBundle\Connection
{
    public function post($url, $data, array $options = null)
    {
        return false;
    }
}

class CurlMock_ResponseValue_InvalidJson extends \GorillaHub\CurlBundle\Connection
{
    public function post($url, $data, array $options = null)
    {
        return '{"@type":"test"}';
    }
}

class CurlMock_ResponseValue_ExceptionTransmitter extends \GorillaHub\CurlBundle\Connection
{
    public function post($url, $data, array $options = null)
    {
        $exception = new ExceptionTransmitter();
        $exception->setMessage('Exception transmitter test message.');
        $exception->setCode(500);

        $json = new JSONSerializer();

        return $json->serialize($exception);
    }
}

class CurlMock_ResponseValue extends \GorillaHub\CurlBundle\Connection
{
    public function post($url, $data, array $options = null)
    {
        $preview = new Preview();
        $preview->setDuration(10);
        $preview->setDefaultDuration(20);
        $preview->setStartTime(5);

        $json = new JSONSerializer();

        return $json->serialize($preview);
    }
}