<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\ExceptionTransmitter;

class ExceptionTransmitterTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingCodeValue()
    {
        $exception = new ExceptionTransmitter();

        $code = 100;

        $exception->setCode($code);

        $this->assertEquals($code, $exception->getCode());
    }

    public function testSettingTypeValue()
    {
        $exception = new ExceptionTransmitter();

        $type = 'Object';

        $exception->setType($type);

        $this->assertEquals($type, $exception->getType());
    }

    public function testSettingMessageValue()
    {
        $exception = new ExceptionTransmitter();

        $message = 'Message';

        $exception->setMessage($message);

        $this->assertEquals($message, $exception->getMessage());
    }

    public function testSettingLineValue()
    {
        $exception = new ExceptionTransmitter();

        $line = 10;

        $exception->setLine($line);

        $this->assertEquals($line, $exception->getLine());
    }

    public function testSettingFileValue()
    {
        $exception = new ExceptionTransmitter();

        $file = 'file.php';

        $exception->setFile($file);

        $this->assertEquals($file, $exception->getFile());
    }

}