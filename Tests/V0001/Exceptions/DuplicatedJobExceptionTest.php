<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\DuplicatedJobException;

class DuplicatedJobExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'The Job already exists.';

        $exception = new DuplicatedJobException();

        $this->assertEquals($expected, $exception->getMessage());
    }

}