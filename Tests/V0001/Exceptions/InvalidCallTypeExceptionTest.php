<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidCallTypeException;

class InvalidCallTypeExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'The Job already exists.';

        $exception = new InvalidCallTypeException();

        $this->assertEquals($expected, $exception->getMessage());
    }
}

