<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;


use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\UnableToSaveTheUploadFileException;

class UnableToSaveTheUploadFileExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'Unable to save the upload file.';

        $exception = new UnableToSaveTheUploadFileException();

        $this->assertEquals($expected, $exception->getMessage());
    }
} 