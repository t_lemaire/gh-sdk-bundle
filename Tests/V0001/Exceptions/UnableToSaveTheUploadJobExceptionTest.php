<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\UnableToSaveTheUploadJobException;

class UnableToSaveTheUploadJobExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'Unable to save the upload job.';

        $exception = new UnableToSaveTheUploadJobException();

        $this->assertEquals($expected, $exception->getMessage());
    }

}