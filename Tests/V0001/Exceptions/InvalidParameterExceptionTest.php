<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class InvalidParameterExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'Invalid parameter.';

        $exception = new InvalidParameterException();

        $this->assertEquals($expected, $exception->getMessage());
    }

}