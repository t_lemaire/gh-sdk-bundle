<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidResponseException;

class InvalidResponseExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'Invalid response.';

        $exception = new InvalidResponseException();

        $this->assertEquals($expected, $exception->getMessage());
    }

}