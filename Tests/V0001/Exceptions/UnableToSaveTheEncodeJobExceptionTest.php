<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\UnableToSaveTheEncodeJobException;

class UnableToSaveTheEncodeJobExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'Unable to save the encode job.';

        $exception = new UnableToSaveTheEncodeJobException();

        $this->assertEquals($expected, $exception->getMessage());
    }

} 