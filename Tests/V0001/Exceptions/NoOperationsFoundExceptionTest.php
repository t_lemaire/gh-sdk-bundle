<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\NoOperationsFoundException;

class NoOperationsFoundExceptionTest extends \PHPUnit_Framework_TestCase
{

    public function testHavingDefaultMessageValue()
    {
        $expected = 'No operations found.';

        $exception = new NoOperationsFoundException();

        $this->assertEquals($expected, $exception->getMessage());
    }

}