<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\OriginalInfoParameter;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\VideoResult;

class VideoResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingAspect()
    {
        $result = new VideoResultMock();

        $originalInfo = new OriginalInfoParameter();
        $originalInfo->setFileName('file.mp4');

        $result->setOriginalInfo($originalInfo);
        $this->assertEquals($originalInfo, $result->getOriginalInfo());
    }

}

class VideoResultMock extends VideoResult
{

}

