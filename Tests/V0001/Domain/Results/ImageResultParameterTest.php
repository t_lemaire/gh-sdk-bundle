<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats\ImageFormats;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\ImageBased\PatternRange;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\ImageBasedResult;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\ImageResultParameter;

class ImageResultParameterTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingAspect()
    {
        $result = new ImageResultParameter();

        $aspect = 10;

        $result->setAspect($aspect);
        $this->assertEquals($aspect, $result->getAspect());
    }

    public function testSettingCount()
    {
        $result = new ImageResultParameter();

        $count = 10;

        $result->setCount($count);
        $this->assertEquals($count, $result->getCount());
    }

    public function testSettingFormat()
    {
        $result = new ImageResultParameter();

        $result->setFormat(ImageFormats::FORMAT_JPG);
        $this->assertEquals(ImageFormats::FORMAT_JPG, $result->getFormat());
    }

    public function testSettingHeight()
    {
        $result = new ImageResultParameter();

        $height = 10;

        $result->setHeight($height);
        $this->assertEquals($height, $result->getHeight());
    }

    public function testSettingWidth()
    {
        $result = new ImageResultParameter();

        $width = 10;

        $result->setWidth($width);
        $this->assertEquals($width, $result->getWidth());
    }

    public function testSettingNamePrefix()
    {
        $result = new ImageResultParameter();

        $prefix = 'porn';

        $result->setNamePrefix($prefix);
        $this->assertEquals($prefix, $result->getNamePrefix());
    }

    public function testSettingNameSuffix()
    {
        $result = new ImageResultParameter();

        $suffix = 'xxx';

        $result->setNameSuffix($suffix);
        $this->assertEquals($suffix, $result->getNameSuffix());
    }

    public function testSettingPatterRange()
    {
        $result = new ImageResultParameter();

        $pattern = new PatternRange();
        $pattern->setStart(1);
        $pattern->setEnd(10);

        $result->setPatternRange($pattern);
        $this->assertEquals($pattern, $result->getPatternRange());
    }

    public function testSettingPatternUrl()
    {
        $result = new ImageResultParameter();

        $pattern = 'Url pattern??';

        $result->setPatternUrl($pattern);
        $this->assertEquals($pattern, $result->getPatternUrl());
    }

    public function testSettingQuality()
    {
        $result = new ImageResultParameter();

        $quality = 100;

        $result->setQuality($quality);
        $this->assertEquals($quality, $result->getQuality());
    }

    public function testGetStart()
    {
        $pattern = new PatternRange();
        $pattern->setStart(1);
        $this->assertEquals(1, $pattern->getStart());
    }

    public function testGetEnd()
    {
        $pattern = new PatternRange();
        $pattern->setEnd(10);
        $this->assertEquals(10, $pattern->getEnd());
    }

}

class ImageBasedResultMock extends ImageBasedResult
{

}
