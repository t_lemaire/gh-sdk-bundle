<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\ErrorResult;


class ErrorResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingMessageValue()
    {
        $expected = 'An error happened.';

        $result = new ErrorResult();

        $result->setMessage($expected);
        $this->assertEquals($expected, $result->getMessage());
    }

    public function testSettingMetadataValue()
    {
        $expected = array('1' => 2, 'foo' => 'bar');

        $result = new ErrorResult();

        $result->setMetadata($expected);
        $this->assertEquals($expected, $result->getMetadata());
    }

}
