<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\FailResult;

class FailResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingStatusValue()
    {
        $expected = 'fail';

        $result = new FailResult();

        $result->setStatus($expected);
        $this->assertEquals($expected, $result->getStatus());
    }

    public function testSettingReasonValue()
    {
        $expected = 'It jus failed';

        $result = new FailResult();

        $result->setReason($expected);
        $this->assertEquals($expected, $result->getReason());
    }

    public function testSettingMessageValue()
    {
        $expected = 'An error happened.';

        $result = new FailResult();

        $result->setMessage($expected);
        $this->assertEquals($expected, $result->getMessage());
    }

}
