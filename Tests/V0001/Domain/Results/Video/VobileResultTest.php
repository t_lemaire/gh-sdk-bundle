<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\VobileResult;

class VobileResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingResultValue()
    {
        $expected = 'value';

        $result = new VobileResult();

        $result->setResult($expected);
        $this->assertEquals($expected, $result->getResult());
    }

}

