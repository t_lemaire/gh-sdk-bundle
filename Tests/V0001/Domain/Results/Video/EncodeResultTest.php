<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results\Video;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats\VideoFormats;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\EncodeResult;

class EncodeResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingAspect()
    {
        $param = new EncodeResult();

        $code = 10;

        $param->setAspect($code);
        $this->assertEquals($code, $param->getAspect());
    }

    public function testSettingAudioBitRate()
    {
        $param = new EncodeResult();

        $bitRate = 10;

        $param->setAudioBitRate($bitRate);
        $this->assertEquals($bitRate, $param->getAudioBitRate());
    }

    public function testSettingAudioChannel()
    {
        $param = new EncodeResult();

        $channel = 10;

        $param->setAudioChannel($channel);
        $this->assertEquals($channel, $param->getAudioChannel());
    }


    public function testSettingDuration()
    {
        $param = new EncodeResult();

        $duration = 10;

        $param->setDuration($duration);
        $this->assertEquals($duration, $param->getDuration());
    }

    public function testSettingFileName()
    {
        $param = new EncodeResult();

        $name = 'video.mp4';

        $param->setFileName($name);
        $this->assertEquals($name, $param->getFileName());
    }

    public function testSettingFileSize()
    {
        $param = new EncodeResult();

        $size = 123;

        $param->setFileSize($size);
        $this->assertEquals($size, $param->getFileSize());
    }

    public function testSettingFormat()
    {
        $param = new EncodeResult();

        $format = VideoFormats::FORMAT_MP4;

        $param->setFormat($format);
        $this->assertEquals($format, $param->getFormat());
    }

    public function testSettingHeight()
    {
        $param = new EncodeResult();

        $height = 100;

        $param->setHeight($height);
        $this->assertEquals($height, $param->getHeight());
    }

    public function testSettingWidth()
    {
        $param = new EncodeResult();

        $width = 100;

        $param->setWidth($width);
        $this->assertEquals($width, $param->getWidth());
    }

    public function testSettingResolution()
    {
        $param = new EncodeResult();

        $resolution = 720;

        $param->setResolution($resolution);
        $this->assertEquals($resolution, $param->getResolution());
    }


    public function testSettingVideoBitRate()
    {
        $param = new EncodeResult();

        $rate = 100;

        $param->setVideoBitRate($rate);
        $this->assertEquals($rate, $param->getVideoBitRate());
    }

    public function testSettingNamePrefix()
    {
        $result = new EncodeResult();

        $prefix = 'porn';

        $result->setNamePrefix($prefix);
        $this->assertEquals($prefix, $result->getNamePrefix());
    }

    public function testSettingNameSuffix()
    {
        $result = new EncodeResult();

        $suffix = 'xxx';

        $result->setNameSuffix($suffix);
        $this->assertEquals($suffix, $result->getNameSuffix());
    }

    public function testSettingPreviewUrl()
    {
        $result = new EncodeResult();

        $url = 'http://xxx';

        $result->setPreviewUrl($url);
        $this->assertEquals($url, $result->getPreviewUrl());
    }

    public function testSettingUrl()
    {
        $result = new EncodeResult();

        $url = 'http://xxx';

        $result->setUrl($url);
        $this->assertEquals($url, $result->getUrl());
    }

}

