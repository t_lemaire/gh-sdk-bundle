<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results\Video\Parameters;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats\VideoFormats;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\Parameters\OriginalInfoParameter;

class OriginalInfoParameterTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingAspect()
    {
        $param = new OriginalInfoParameter();

        $code = 10;

        $param->setAspect($code);
        $this->assertEquals($code, $param->getAspect());
    }

    public function testSettingAudioBitRate()
    {
        $param = new OriginalInfoParameter();

        $bitRate = 10;

        $param->setAudioBitRate($bitRate);
        $this->assertEquals($bitRate, $param->getAudioBitRate());
    }

    public function testSettingAudioChannel()
    {
        $param = new OriginalInfoParameter();

        $channel = 10;

        $param->setAudioChannel($channel);
        $this->assertEquals($channel, $param->getAudioChannel());
    }

    public function testSettingAudioCodec()
    {
        $param = new OriginalInfoParameter();

        $codec = 'ACC';

        $param->setAudioCodec($codec);
        $this->assertEquals($codec, $param->getAudioCodec());
    }

    public function testSettingAudioSampleRate()
    {
        $param = new OriginalInfoParameter();

        $rate = '10';

        $param->setAudioSampleRate($rate);
        $this->assertEquals($rate, $param->getAudioSampleRate());
    }

    public function testSettingDuration()
    {
        $param = new OriginalInfoParameter();

        $duration = 10;

        $param->setDuration($duration);
        $this->assertEquals($duration, $param->getDuration());
    }

    public function testSettingFileExtension()
    {
        $param = new OriginalInfoParameter();

        $extension = '.mp4';

        $param->setFileExtension($extension);
        $this->assertEquals($extension, $param->getFileExtension());
    }

    public function testSettingFileName()
    {
        $param = new OriginalInfoParameter();

        $name = 'video.mp4';

        $param->setFileName($name);
        $this->assertEquals($name, $param->getFileName());
    }

    public function testSettingFileSize()
    {
        $param = new OriginalInfoParameter();

        $size = 123;

        $param->setFileSize($size);
        $this->assertEquals($size, $param->getFileSize());
    }

    public function testSettingFormat()
    {
        $param = new OriginalInfoParameter();

        $format = VideoFormats::FORMAT_MP4;

        $param->setFormat($format);
        $this->assertEquals($format, $param->getFormat());
    }

    public function testSettingHeight()
    {
        $param = new OriginalInfoParameter();

        $height = 100;

        $param->setHeight($height);
        $this->assertEquals($height, $param->getHeight());
    }

    public function testSettingWidth()
    {
        $param = new OriginalInfoParameter();

        $width = 100;

        $param->setWidth($width);
        $this->assertEquals($width, $param->getWidth());
    }

    public function testSettingResolution()
    {
        $param = new OriginalInfoParameter();

        $resolution = 720;

        $param->setResolution($resolution);
        $this->assertEquals($resolution, $param->getResolution());
    }

    public function testSettingRotation()
    {
        $param = new OriginalInfoParameter();

        $rotation = 0;

        $param->setRotation($rotation);
        $this->assertEquals($rotation, $param->getRotation());
    }

    public function testSettingVideoBitRate()
    {
        $param = new OriginalInfoParameter();

        $rate = 100;

        $param->setVideoBitRate($rate);
        $this->assertEquals($rate, $param->getVideoBitRate());
    }

    public function testSettingVideoCodec()
    {
        $param = new OriginalInfoParameter();

        $codec = 'mpg';

        $param->setVideoCodec($codec);
        $this->assertEquals($codec, $param->getVideoCodec());
    }

}
