<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain\Results;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use GorillaHub\SDKs\Tests\Traits\ClientId;
use GorillaHub\SDKs\Tests\Traits\JobId;

class ResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingSignatureValue()
    {
        $signature = new Signature();

        $result = new ResultMock();
        $result->setSignature($signature);

        $this->assertEquals($signature, $result->getSignature());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result::setSignature() must be an instance of GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature, null given
     */
    public function testSettingSignatureValueException()
    {
        $result = new ResultMock();
        $result->setSignature(null);
    }

    public function testSettingOriginalOperationValue()
    {
        $originalOperation = new EncodeOperation();

        $result = new ResultMock();
        $result->setOriginalOperation($originalOperation);
        $this->assertEquals($originalOperation, $result->getOriginalOperation());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result::setOriginalOperation() must implement interface GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface, null given
     */
    public function testSettingOriginalOperationValueException()
    {
        $result = new ResultMock();
        $result->setOriginalOperation(null);
    }

}


class ResultMock extends Result
{
}