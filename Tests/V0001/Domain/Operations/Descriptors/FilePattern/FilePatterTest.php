<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001\Domain\Operations\Descriptors\FilePattern;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;

class TestFilePattern extends \PHPUnit_Framework_TestCase
{

    public function testSettingBasePathValue()
    {
        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
        $filePattern = new FilePattern();

        $basePath = 'local';

        $filePattern->setBasePath($basePath);
        $this->assertEquals($basePath, $filePattern->getBasePath());
    }


    public function testSettingFilePatternValue()
    {
        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
        $filePattern = new FilePattern();

        $pattern = '%y%m%d';

        $filePattern->setFilePattern($pattern);
        $this->assertEquals($pattern, $filePattern->getFilePattern());
    }

    public function testSettingPathPatternValue()
    {
        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
        $filePattern = new FilePattern();

        $pattern = '%y%m%d';

        $filePattern->setPathPattern($pattern);
        $this->assertEquals($pattern, $filePattern->getPathPattern());
    }

    public function testParsingPathPatternValue()
    {
        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
        $filePattern = new FilePattern();

        $pattern = '%y%m%d';

        $filePattern->setPathPattern($pattern);

        $this->assertEquals($pattern, $filePattern->parsePathPattern());
    }

    public function testParsingFilePatternValue()
    {
        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
        $filePattern = new FilePattern();

        $pattern = '%y%m%d';

        $filePattern->setFilePattern($pattern);

        $this->assertEquals($pattern, $filePattern->parseFilePattern());
    }
}
