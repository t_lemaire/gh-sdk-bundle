<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001\Domain\Operations;


use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use GorillaHub\SDKs\UploadBundle\V0001\Domain\Operations\StoreInTheOriginOperation;

class StoreInTheOriginOperationTest extends \PHPUnit_Framework_TestCase
{

    public function testSetFilePattern()
    {
        /* @var StoreInTheOriginOperation */
        $storeInTheOriginOperation = new StoreInTheOriginOperation();
        /* @var FilePattern */
        $filePattern = new FilePattern();
        $storeInTheOriginOperation->setFilePattern($filePattern);
        $this->assertEquals($filePattern, $storeInTheOriginOperation->getFilePattern());
    }
} 