<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

class CallTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingSignatureValue()
    {
        $signature = new Signature();

        $call = new CallMock();
        $call->setSignature($signature);
        $this->assertEquals($signature, $call->getSignature());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call::setSignature() must be an instance of GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature, null given
     */
    public function testSettingSignatureValueException()
    {
        $result = new CallMock();
        $result->setSignature(null);
    }

}

class CallMock extends Call
{
}