<?php

namespace GorillaHub\SDKs\UploadBundle\Tests\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\SuccessCall;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\EncodeResult;
use GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video\VideoUploadJob;

class SuccessCallTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingOriginalCall()
    {
        $originalCall = new VideoUploadJob();

        $call = new SuccessCall();
        $call->setOriginalCall($originalCall);
        $this->assertEquals($originalCall, $call->getOriginalCall());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call::setOriginalCall() must implement interface GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface, null given
     */
    public function testSettingOriginalCallNullValue()
    {
        $call = new SuccessCall();
        $call->setOriginalCall(null);
    }

    public function testSettingResult()
    {
        $result = new EncodeResult();

        $call = new SuccessCall();
        $call->setResult($result);
        $this->assertEquals($result, $call->getResult());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\SDKBundle\V0001\Domain\Calls\Call::setResult() must implement interface GorillaHub\SDKs\SDKBundle\V0001\Domain\ResultInterface, null given
     */
    public function testSettingResultNullValue()
    {
        $call = new SuccessCall();
        $call->setResult(null);
    }

}
