<?php

namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class SignatureTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingClientIdValue()
    {
        $clientId = 10;

        $signature = new Signature();
        $signature->setClientId($clientId);
        $this->assertEquals($clientId, $signature->getClientId());

        $clientId = '10';

        $signature->setClientId($clientId);
        $this->assertEquals((int)$clientId, $signature->getClientId());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Client Id is not valid.
     */
    public function testSettingClientIdValueException()
    {
        $signature = new Signature();

        try {
            $signature->setClientId(null);
        } catch (InvalidParameterException $e) {
            throw $e;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testSettingJobIdValue()
    {
        $id = md5('a');

        $signature = new Signature();

        $signature->setJobId($id);
        $this->assertEquals($id, $signature->getJobId());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Id has to be a string value.
     */
    public function testSettingJobIdNullValueException()
    {
        $signature = new Signature();

        try {
            $signature->setJobId(null);
        } catch (InvalidParameterException $e) {
            throw $e;
        }

        $this->fail('An expected exception has not been raised.');
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Job Id has to be 32 characters long.
     */
    public function testSettingJobIdWrongValueLengthException()
    {
        $signature = new Signature();

        try {
            $signature->setJobId('123');
        } catch (InvalidParameterException $e) {
            throw $e;
        }

        $this->fail('An expected exception has not been raised.');
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Invalid Job Id, it must contain any combination of numbers from 0-9 and lower case letters from a-f only.
     */
    public function testSettingJobIdWrongValueFormatException()
    {
        $signature = new Signature();

        try {
            $signature->setJobId('123456789012345678901234567890!a');
        } catch (InvalidParameterException $e) {
            throw $e;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testSettingTransactionIdValue()
    {
        $signature = new Signature();

        $transactionId = '1234';
        $signature->setTransactionId($transactionId);
        $this->assertEquals($transactionId, $signature->getTransactionId());
    }

}