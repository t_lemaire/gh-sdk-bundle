<?php

namespace GorillaHub\SDKs\SDKBundle\Tests\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTime;

class DateTimeTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingISO8601Value()
    {
        $expected = '2014-03-04T17:56:34+0000';

        $datetime = new DateTime();

        $datetime->setISO8601($expected);
        $this->assertEquals($expected, $datetime->getISO8601());
    }

    public function testConvertingToDateTime()
    {
        $iso8601 = '2014-03-04T17:56:34+0000';

        $obj = new DateTime();
        $obj->setISO8601($iso8601);

        $dateTime = new \DateTime($iso8601);

        $this->assertEquals($dateTime, $obj->toDateTime());
    }
} 